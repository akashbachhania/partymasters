<html>

<head>
<title>Gallery | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<script src="pmaz.js"></script>
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
    <script src="slider.js" type="text/javascript"></script>
	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
</head>

<body>
<div class="header2" style="margin-top:0px;">
<img id="navback" src="headerback.png"/>
<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>
<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" style="opacity:1;" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>
</center>

</div>

</div>


<center>


</div>
<div class="gallerypage">

<div class="gnav">
<ul>
<li id="back"><a href="gallery.php"><i class="fa fa-undo"></i> Back to Gallery</a></li>
<li id="active"><a href=""><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href=""><i class="fa fa-search-plus"></i> Photography</a></li>
<li><a href=""><i class="fa fa-search-plus"></i> Bar/Bat Mitzvah</a></li>
<li><a href=""><i class="fa fa-search-plus"></i> Photo Montage</a></li>
<li><a href=""><i class="fa fa-search-plus"></i> DiscJockeys</a></li>
<li><a href=""><i class="fa fa-search-plus"></i> Decoration</a></li>
<li><a href=""><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href=""><i class="fa fa-search-plus"></i> Video Dance</a></li>
<li><a href=""><i class="fa fa-search-plus"></i> Kids Parties</a></li>
</ul>
</div>

<center>
<div id="blackfade">
<div id="1">
<div id="1" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('1');"><i class="fa fa-times-circle-o"></i></a>
<img src="wedding.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(2);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(13);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="2">
<div id="2" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('2');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(3);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(1);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="3">
<div id="3" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('3');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner2.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(4);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(2);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="4">
<div id="4" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('4');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner3.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(5);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(3);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="5">
<div id="5" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('5');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner4.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(6);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(4);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="6">
<div id="6" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('6');"><i class="fa fa-times-circle-o"></i></a>
<img src="box1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(7);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(5);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="7">
<div id="7" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('7');"><i class="fa fa-times-circle-o"></i></a>
<img src="boxbanner.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(8);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(6);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="8">
<div id="8" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('8');"><i class="fa fa-times-circle-o"></i></a>
<img src="img35.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(9);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(7);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="9">
<div id="9" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('9');"><i class="fa fa-times-circle-o"></i></a>
<img src="memories.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(10);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(8);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="10">
<div id="10" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('10');"><i class="fa fa-times-circle-o"></i></a>
<img src="w1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(11);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(9);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="11">
<div id="11" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('11');"><i class="fa fa-times-circle-o"></i></a>
<img src="w1.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(12);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(10);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="12">
<div id="12" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('12');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(13);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(11);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="13">
<div id="13" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('13');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner4.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(14);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(12);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="14">
<div id="14" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('14');"><i class="fa fa-times-circle-o"></i></a>
<img src="redback.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(15);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(13);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="15">
<div id="15" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('15');"><i class="fa fa-times-circle-o"></i></a>
<img src="back2.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(16);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(14);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="16">
<div id="16" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('16');"><i class="fa fa-times-circle-o"></i></a>
<img src="w2.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(17);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(15);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="17">
<div id="17" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('17');"><i class="fa fa-times-circle-o"></i></a>
<img src="w3.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(18);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(16);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="18">
<div id="18" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('18');"><i class="fa fa-times-circle-o"></i></a>
<img src="ball.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(19);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(17);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="19">
<div id="19" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('19');"><i class="fa fa-times-circle-o"></i></a>
<img src="footerback.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(20);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(18);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="20">
<div id="20" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('20');"><i class="fa fa-times-circle-o"></i></a>
<img src="jazz.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(1);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(19);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>

</div>
</center>
<div class="galleryitems">
<h1 style="text-align:left;">Our Wedddings Gallery</h1>
<h2 style="text-align:left;">Making Memories Happen</h2>
</div>
<div class="gallerylist">
<ul>
<li>
<img onclick="Showbox('1');"  class="small" src="wedding.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('2');" class="small" src="banner1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('3');"  class="small" src="banner2.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('4');"  class="small" src="banner3.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('5');"  class="small" src="banner4.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('6');"  class="small" src="box1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('7');"  class="small" src="boxbanner.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('8');"  class="small" src="img35.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('9');"  class="small" src="memories.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('10');" class="small" src="w1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('11');" class="small" src="banner.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('12');" class="small" src="gallery.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('13');" class="small" src="banner4.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('14');" class="small" src="redback.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('15');" class="small" src="back2.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('16');" class="small" src="w2.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('17');" class="small" src="w3.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('18');" class="small" src="ball.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('19');" class="small" src="footerback.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('19');" class="small" src="jazz.jpg"><i class="fa fa-search-plus"></i>
</li>
</ul>
</div>
</div>
</div>
</center>
<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>


</body>

</html>