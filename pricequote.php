<html>
<head>
<title>PriceQuote | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<script src="jqueryslide.js"></script>
<script src="pmaz.js"></script>
<script src="slidecontrol.js"></script>
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>

	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>

</head>

<body>

<header>

<div class="header2">

<img id="navback" src="headerback.png"/>



<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>

<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>

</center>

</div>

</div>

<img id="logotop" src="logo.png"/>

</header>

<div id="container">
	<ul>
      	<li><img src="banner.png" width="100%" /><h5>sub caption</h5></li>
      	<li><img src="banner1.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner2.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner3.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner4.jpg" width="100%" /><h5>sub caption</h5></li>

      </ul>
<h1>Price Quote</h1>	  
      <span class="button prevButton"></span>
      <span class="button nextButton"></span>
</div>
<center>
</div>
<div class="leftbar">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>
</div>
<div class="rightside">

<div class="addbooth">
<h1>Memorable Events</h1>
<p>Party Masters provides all the entainment services you need for unforgettable events!</p>
<h2>More Services = More Savings!</h2>

<h3>Party Masters Services</h3>
<ul>
<li style="background-color:#22B14C;"><img src="iconset/booth.png"/> Photo Booths</li>
<li style="background-color:#B200FF;"><img src="iconset/dj.png"/> Disc Jockeys</li>
<li style="background-color:#29A2D2;"><img src="iconset/camera.png"/> Photographers</li>
<li style="background-color:#FF7F27;"><img src="iconset/led.png"/> LED Lighting</li>
<li style="background-color:#7F0000;"><img src="iconset/montage.png"/> Photo Montage</li>
<li style="background-color:#FF006E;"><img src="iconset/video.png"/> Videographers</li>
<li style="background-color:#002F86;"><img src="iconset/party.png"/> Event Rentals</li>
<li style="background-color:#002F86;"><img src="iconset/dance.png"/> Dance Floors</li>
<li style="background-color:#002F86;"><img src="iconset/projectors.png"/> Screens Projectors</li>
<li style="background-color:#002F86;"><img src="iconset/partyplan.png"/> Party Planning</li>

</ul>
<img src="logo.png"/>

<a id="number" href="tel:1-480-947-6500">(480) 947-6500</a>
</div>
</div>
<div class="whitecenter">
<h1 id="cntnt">Get A Free Price Quote</h1>

<p id="cntnt">At Party Masters Entertainment, before we provide you with a “number” we need to know a few details about your event so we can provide you with our BEST PRICE QUOTE. We develop and customize various packages based on your needs. Each event is unique, so providing us with the details of your event (day/time/needs), will only help your chances of getting our BEST PRICE. We provide DISCOUNTS on certain days and times of the year plus, if you’re hiring us for additional services, we provide MULTI-SERVICE DISCOUNTS! This way, you only pay for what you need and our price quotes are uniquely based on your event, size, number of guests, location, services, wants and needs.Many entertainment companies offer their potential clients numerous entertainment packages that the client must sort through to determine what will be best “package” for them.
<br><br>
Prior to providing a price quote for your special occasion/event, we need to gather all the information pertaining to your event. This way, there are no last minute surprises or unexpected costs to deal with. Once we receive this information & review it with you, we will provide you with recommendations that we believe will work best for you based on the information you provided and quote you a fair and competitive price (with no hidden fees).</p>
<div style="width:70%;padding:0">
<div class="selectservice">
<h1>Check Availability and get a Price Quote<br>Select your event date</h1>

<form id="selectservice" method="post" action="pricequote.php">
<input type="date" name="date" required/><br>
<input type="submit" id="selectevent" value="Quote My Event"/>
</form>
</div></div>
<h1 id="cntnt">Did you know?</h1>
<p id="cntnt"> Using Party Masters for multiple services (Disc Jockey, Photography, Videography, or Photo Montage – Slide Show (PixVid™) will REDUCE your overall cost per service. We offer MULTI-service DISCOUNTS!
<br><br>
Party Masters does not quote generic fees and pricing for DJ Entertainment services on our site, since there is a lot more involved and planning. We believe that if the client is truly interested in high quality entertainment, they will take a few minutes with our company representatives to see what we offer. Clients who are only focused on the price are limiting themselves and their ability to find top quality entertainment and first class customer service.
<br><br>
Confirming your event with Party Masters is simple. All of our events require a deposit & signed agreement to confirm your date. We take all major credit cards and PayPal. All travel*, set up (1-2 hours prior to your start time) and break down of equipment is completed on our time – You do not get charged for this!
<br><br>
*Travel fees are subject to your event location if located outside the Phoenix metropolitan area (Tucson, Flagstaff, Sedona, Prescott, Payson, Casa Grade, or any city located 50 miles or more from the center of the city of Phoenix.)
<br><br>
We strongly suggest taking time to meet with us in person (or any entertainment or event service) prior to just getting a price over the internet or phone. Party Masters offers you a FREE NO-OBLIGATION consultation to you and your family or organization. Most importantly, Party Masters works out of an office, NOT from a home, apartment, or out of a car. Our offices are conveniently located in Tempe/Chandler off the 101 freeway and US 60 (appointments only). We also have satellite offices in Scottsdale, or one of our sales team members can meet at a location of your choice. Just ask!
<br></p><br>
</div>






<center>
<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>
</body>
</html>