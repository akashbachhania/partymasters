<html>

<head>
<title>FAQs | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
    <script src="slider.js" type="text/javascript"></script>
	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>
     <script type="text/javascript" language="javascript">
         $(function () {
             var $win = $(window);

             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                     document.getElementById('navback').style.opacity = 0.7;    
             });
			 
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logotop').style.opacity = 1;
			 });
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logoscroll').style.opacity = 0;
			 });
         });
    </script>
</head>

<body>

<div class="header2" style="margin-top:0px;">
<img id="navback" src="headerback.png"/>
<center>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" style="opacity:1;" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>
</center>

</div>

</div>



<center>


<script type="text/javascript">
(function(){
var i = 1;
var start = true;   
    setInterval(function(){
        if (i>0&&start){
         var o = $("#light").css("opacity");
        
            var s = parseFloat(o) - 0.6;
       
            $("#light").css("opacity", s.toString());
           i = s;    
        }
    
        else {
        start = false;
         var o = $("#light").css("opacity");
        var s = parseFloat(o) + 0.6;
       
         $("#light").css("opacity", s.toString());
        i = s; 
            if(s > 1) start = true;
        
        
        }
        
   
   
   
    }, 200);

 
})();

</script>
</div>
<br><br><br><br>
</div>

<div class="leftbar">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>
</div>
<div class="rightside">

<div class="addbooth">
<h1>Memorable Events</h1>
<p>Party Masters provides all the entainment services you need for unforgettable events!</p>
<h2>More Services = More Savings!</h2>

<h3>Party Masters Services</h3>
<ul>
<li style="background-color:#22B14C;"><img src="iconset/booth.png"/> Photo Booths</li>
<li style="background-color:#B200FF;"><img src="iconset/dj.png"/> Disc Jockeys</li>
<li style="background-color:#29A2D2;"><img src="iconset/camera.png"/> Photographers</li>
<li style="background-color:#FF7F27;"><img src="iconset/led.png"/> LED Lighting</li>
<li style="background-color:#7F0000;"><img src="iconset/montage.png"/> Photo Montage</li>
<li style="background-color:#FF006E;"><img src="iconset/video.png"/> Videographers</li>
<li style="background-color:#002F86;"><img src="iconset/party.png"/> Event Rentals</li>
<li style="background-color:#002F86;"><img src="iconset/dance.png"/> Dance Floors</li>
<li style="background-color:#002F86;"><img src="iconset/projectors.png"/> Screens Projectors</li>
<li style="background-color:#002F86;"><img src="iconset/partyplan.png"/> Party Planning</li>

</ul>
<img src="logo.png"/>

<a id="number" href="tel:1-480-947-6500">(480) 947-6500</a>
</div>
</div>
<div class="whitecenter">
<div class="faqs">
<h1>FAQs</h1>
<h2>The following are the MOST FREQUENTLY ASKED questions� Enjoy!<h2>
<h3>FAQ 1� WHY PARTY MASTERS ENTERTAINMENT?</h3>
<p>Simply and most importantly, we listen to YOU. We are highly regarded and respected for helping our clients with all minor and major details of their event and most importantly, help you save money and stay within your budget. Our expert, award winning staff have years of experience to entertain, photograph, or video you and your guests at any Wedding, Bar Mitzvah, Bat Mitzvah, Birthday Party, Anniversary, School event, Company events and much more. We also have been praised for our outstanding customer service in our office and that is very important to consider since all of the planning must be nailed down and organized well before your event date. It�s VERY important you hire a company that has experience and passion for what they offer. Our knowledgeable representatives will spend the necessary time with you throughout the planning stages, and our professional entertainers will treat you and your guests with first class personal service until the last minute of your event. Our entire staff of experienced, knowledgeable, & professional MC�s, music DJ / VJ entertainers, Photographers, Videographers, Photo Booth Operators and party planners know how to make your event an event to remember. We hand select our DJ entertainment staff, photographers and videographers. We provide individually training (if needed), so they are at the top of their game for each event. Most importantly, all of our Phoenix music DJ entertainers, Phoenix Photographers, Phoenix Videographers, Phoenix Photo Booths with green screen have years of experience and really enjoy performing at Party Masters events. Best of all, everyone benefits (especially you and your guests) from the fun & passion each DJ entertainer and staff member brings with them to each event, making your event a huge success and memorable! Bottom line� WE MAKE IT HAPPEN!</p>

<h3>FAQ 2�HOW MUCH EMPHASIS SHOULD I PUT INTO THE MUSIC AND HIRING A DJ SERVICE IN ARIZONA?</h3>
<p>The music entertainment is the MOST IMPORTANT part of any event. It�s a fact, shopping for the cheapest price for an entertainment service will directly affect all of the other services you spent time and money to hire. The Phoenix DJ entertainer is basically the head conductor at your event and each decision he/she makes will directly affect the �vibe� of party. So, it�s important the �conductor� knows how to conduct a successful party. Bottom line: The DJ will make or break the ultimate success of your event. If the entertainment or Phoenix DJ service is inadequate and/or inexperienced (appearance, DJ sound equipment, party music selections, equipment, lighting, etc.), your event will most likely be a complete failure! Keep in mind, a cheap and inexperienced DJ service in Arizona will directly affect ALL of the other services included at your event, no matter how good they are. The quality and experience of the DJ music entertainment you hire at your Arizona Wedding, Phoenix Wedding, Arizona Birthday party, Phoenix Birthday, Phoenix Arizona Mitzvah party, company event, private party or whatever event you are considering in Arizona, is the MOST IMPORTANT decision you will make when hiring outside services for your event. Again, keep in mind, bad entertainment will make the everything at your event, like the food & drinks not taste as good. The Arizona photographer and/or Arizona videographer will have trouble capturing smiling faces & decent/fun pictures when it�s party time. Not to mention, all of the money you spent for the venue, decorations, party coordinator, and anyone else, especially your guests, will greatly suffer. We strongly suggest to ask for references and feedback from past events and clients. Always hire a company that has been in the DJ business in Arizona for many years. Do your homework and take your time not to book the CHEAPEST priced DJ service in Arizona that you talk or meet with in person. Remember, you get what you pay for.</p>

<h3>FAQ 3�WHAT IS THE AVERAGE PRICE FOR A PROFESSIONAL AND EXPERIENCED DISC JOCKEY ENTERTAINMENT SERVICE?</h3>
<p>From our Phoenix area research, a professional mobile disc jockey entertainment service will range from $500.00 to $5,000.00. Price rates vary due to the popularity of your event date, time of the year, the total amount of hours you choose to hire the DJ service, how much equipment we need to bring (for your size event), and most importantly the overall experience, knowledge, professionalism, customer service and years in the business.</p>


<h4>Your event date:</h4>
<p>If your event date lands on a Monday thru Thursday (Holidays excluded), our prices are usually lower compared to an event that lands on a Friday, Saturday, or Sunday (in-season (September thru June). The reason is, the weekends (and special holidays) are very demanding and 95% of the events booked are on these days. You can get the lowest rates and discounts during the summer time (July thru mid September) since there are not as many requested events due to the typical slower season in Arizona because of the very hot weather. The highest prices for ALL types of services are typically on Friday, Saturday, and Sunday nights (and holidays) in peak season (mid September thru Early July). Also, keep in mind, these �peak season� dates are very popular and sell out quickly, so book your venue (location) and your services (DJ Entertainment, Photographer, Videographer, Photo Booth, Rentals, etc.) early so you�re not stuck with nothing for your event. It happens!

The level of Service you select:
The price rate also depends on the total amount of hours you choose to hire the company, the companies or individual overall experience (talent), customer service, professionalism, package/services you choose, creativity, and any additional charges (i.e. Travel, additional equipment (sound & lights), karaoke equipment, rentals (dance floors, tables, chairs, etc.), props, prizes (give-a-ways) dancers, Video Dance parties, video projectors & screens, additional staff, enhanced services, etc.).
</p>
<h4>A VALUABLE TIP:</h4>

<p>Your DJ music entertainment is the most important vendor at your event and usually one of the least expensive compared to the cost of the Venue (location), Caterer (food & drinks), Photographer, Videographer, Florist (decorations), and other various services at most events. If your entertainer is inexperienced and/or incompetent at your event (customer service, music selections, able to �read� the vibe of the event, communication with you and your guests, dressed inappropriately, etc.), or arrives with incomplete and/or non-professional (consumer) equipment (sound system quality & lighting effects)� All of the money you invested for food, decorations, flowers, venue cost, rentals, etc., will be insignificant and a total waste. Additionally, your event will most likely be a complete failure and this is an embarrassment. Keep in mind, you and your guests will always remember how much fun they had (or did not have) at your event.

What will your guest remember about your wedding?
The most important wedding vendor you hire for your Phoenix wedding reception is the Entertainment, a Disc Jockey service. Learn more below.

What is most remembered at the wedding?
What will be REMEMBERED MOST at your wedding reception?
The following information was complied from various national surveys:

74% of all Brides/Grooms say they wish they would have spent MORE TIME selecting their DJ Entertainment company for their wedding reception.
95% said they regret not spending MORE MONEY on the wedding reception entertainment and regret choosing the DJ Entertainment based on the lowest (cheapest) price.
During the wedding planning stages: Brides/Grooms say their highest priority is their attire, followed by the Venue & food selection, and Entertainment was the last item on their list.
When asked, MOST guests say they remember the Entertainment at the wedding. The Entertainment was their basis for determining if the wedding was fun (memorable) or not.
Bottom line:
Your event is an extension of you and the DJ music entertainment/entertainer makes it or breaks it. Do your homework and do not base your decision solely on the cheapest price. You get what you pay for!
</p>
<h3>FAQ 4�DO I NEED TO SIGN AN AGREEMENT?</h3>
<p>YES! To confirm your event with Party Masters, we require you to sign and return our �easy to read and understand� contract (booking) agreement (along with a 50% deposit (we accept all major credit cards). In fact, you should always sign an agreement with any service/vendor you hire. This protects you as well as the company you hire. It�s very important that both side agree on the price and items discussed. Whatever you choose to spend on your Phoenix DJ music entertainment service, Photographer in phoenix, Videographer in Phoenix, Photo Booth company in Phoenix, including all vendors you plan to hire, we strongly suggest you READ YOUR SERVICE AGREEMENT completely, before signing it. Know all terms and as ask any questions if you do not understand or agree with it. If you require something specific, make sure you get it in writing. Understanding the terms of the agreement will help you avoid disappointment on the day of your event.</p>

<h3>FAQ 5 �PRIOR TO MY EVENT, WHAT HAPPENS� DO I NEED TO DO ANYTHING?</h3>
<p>YES, We will provide you with a client account area for your event type (for DJ services only). You should plan on visiting your client area often to add in your music requests, complete the event planner area and timeline. We would like this all completed within 2 weeks prior to your event date so we can review it with your assigned DJ entertainer. About a week prior to your event date, your assigned DJ entertainer will contact you, to review the paperwork you completed online and go over any details regarding your event� the timeline, music requests, and special requests. Plus, anything else regarding the venue, setup/breakdown, music, announcements, etc.. Opening the lines of communication prior to your event between you and your DJ, so there are no last minute surprises or unknowns, is critical to the ultimate success of your event.</p>

<h3>FAQ 6 �WHAT WILL THE DJ PROVIDE AT MY EVENT?</h3>
<p>On your event date, your DJ entertainer will arrive between 1 to 2 hours prior to the start time on the contract agreement. This provides enough time for the DJ to bring the equipment into the performance area, setup and test the equipment (sound and lighting). If you need the equipment setup more than two hours prior to the start time, please contact our offices to make alternative arrangements (Additional charges may apply).

Once all of the equipment is tested and ready to go, the DJ will get organized and prepare for your first guest to arrive. He will begin playing the music program as stated on the agreement.

At your event, your DJ entertainer will play your requested music throughout the event (or at specific times discussed in your meeting). Most importantly, throughout the event, the DJ will continue to �read the crowd� and �feel the vibe� of the room and play the appropriate music (genres), at the appropriate sound level, and make announcements throughout the event. Our entertainers are fully trained to play a variety of music that is appropriate for each changing moment of the event. For example, at weddings, since there are many basic events that need to be conducted (introductions, first dance, bridal party dance, etc.), it�s important the proper music (and music level) is played during each particular event. If needed, the DJ will get involved on the dance floor (interaction) with your guests to either help conduct specific events and/or teach specialty dances.</p>
</p>
<h3>FAQ 7 �WHAT KIND OF SOUND EQUIPMENT DO YOU USE?</h3>
<p>All of our equipment is PROFESSIONAL grade and NOT home/consumer grade. We use a variety of professional (mobile equipment): JBL, EV, Mackie, Pevey brand speakers; Numark, Rane, Pioneer (pro-series) brand mixers/CD/DVD players. Additionally, our systems include an extensive digital music library (all eras of music). Each sound system includes a professional wireless microphone for mobility around the room.
<br><br>
Basic system includes:
<br><br>
2 professional speakers w/ stands
Professional sound mixer w/ computer system (music files)
Wireless handheld microphone
Lighting effects (LED multi-color Lighting effects)
Additional Lighting effects (LED), sound systems & speakers are available upon request (additional charges apply):
<br><br>
*Ceremony sound/music & wireless microphone system (2nd DJ, available if needed. Additional fee applies)
Sub woofer(s) (for additional bass)
Additional speakers (for very large rooms and over 200 guests)
LED lighting effects for the dance floor area and LED up-lighting around the room (walls)
*For Wedding Ceremonies and/or cocktail hours, located in a separate room/area from the main Reception, we offer a complete ceremony sound system including a wireless microphone for the person conducting the ceremony or anyone making a speech or singing a song. No lights or other effects are included in this system setup, unless requested. (Additional charges may apply).
</p>
<h3>FAQ 8 �WHAT KIND OF LED LIGHTING EFFECTS DO YOU HAVE?</h3>
<p>Our Basic lighting System consist of multi-color LED lights that cover the dance floor area.

LED up-lights (any color) for lighting up walls and creating great effects at your event
Eclipse LED (starry Night effect) with moving black light effects (MOST POPULAR)
Revo4 (LED multi-color effects with over 50 color patterns
Monarch LED multi-color effects with over 25 color patterns
UV � LED black lights (Cannon 300w light & LED UV lights)
LED strobe lights
Fog machine
Bubble machine
Starball (replaces a mirror ball effect)
A complete TRUSS lighting system, and more
See photos and videos of these lights on our Facebook and G+ pages: Facebook Page G+ Page

</p>

<h3>FAQ 9 �DO YOU OFFER ADDITIONAL SERVICES?</h3>
<p>YES! Party Masters Entertainment offers a complete line of services (internally), in addition to our Disc Jockey entertainment, such as:
Magicians

Photo Booth (with Green Screen and video recording)
Photography services
Videography Services
PixVid (Photo Montage � Slide Show) productions
LED Lighting effects
Large screen video projection (front or rear)
Truss systems
Balloon Artists
Magicians
Face Painters
Casino tables (w/ chips and dealers)
(additional fees apply for these addtional services)

For complete information & pricing on the above additional services, please contact a representative at the Party Masters Entertainment sales office at info@PartyMastersAZ.com.
</p>
<h3>FAQ 10 �WHEN WILL I BE ASSIGNED AN ENTERTAINER (DJ)?</h3>
<p>If you did not request a specific person, we�ll make our staff assignments 2-4 weeks prior to your event, sometimes earlier. We take special care in matching the proper DJ entertainer to your event. Why? Some entertainers are stronger in certain musical/entertainment areas and we want to match the best person with your event type. Another considering factor is the location and your special requests. If you request a DJ that is very outgoing opposed to less interactive, we will make our assignment based on this in addition to all of the factors we have gathered from the meetings and paperwork we have received to that point. We�ve had much success, at our events, by taking our time to assign a specific DJ entertainer.
</p>
<h3>FAQ 11 �WHEN WILL MY ASSIGNED ENTERTAINER (DJ) CONTACT ME?</h3>
<p>Expect to hear from your assigned DJ entertainer the week prior to your event date. Why? With so many events happening (mostly weekends), we do not want to overload the paperwork and cause confusion. The closer to your event date, the more your entertainer can keep fresh in their mind. Notes will be taken, but again, this system has proven success over the year with our staff and customers like you.
</p>
<h3>FAQ 12�WHAT CRITERIA DO YOU USE TO ASSIGN A DJ?</h3>
<p>After meeting with you in person (or talking to you on the phone), plus reviewing your completed paperwork (music and crowd type), we match the personality of our entertainers to your event. What we mean by this is if you request a wild and crazy DJ that specializes in a certain music type (like country music, rock, kids music), we will assign our most matched DJ entertainer to your event.
</p>
<h3>FAQ 13�WHAT IS YOUR PRIVACY POLICY?</h3>
<p>Any information collected through this site will not be shared with any other parties or entities. We respect your privacy!
</p>
<h3> 14�WHAT IS YOUR REFUND POLICY?</h3>
<p>Once DEPOSIT has been made, if purchaser cancels this agreement for whatever reason, MORE THAN 90 DAYS of the event date, 50% of the deposit shall be refunded.
If Purchaser cancels WITHIN 90 DAYS of the event date, the FULL amount of the deposit will be forfeited.
If Purchaser cancels, WITHIN 60 DAYS prior to the event date, 100% of the total amount will be forfeited. The Purchaser will NOT receive a refund and will be fully
responsible for any remaining balance due (Total price of the agreement) according to the agreement�s TOTAL AMOUNT FOR SERVICES. ALL CANCELLATIONS MUST BE IN WRITING and submitted by the purchaser on the agreement. No verbal cancellations will be accepted.
If Party Masters cancels this agreement for whatever reason, 100% of any monies paid to Party Masters by the Purchaser will be refunded to the Purchaser within 7 days.Please read YOUR agreement for details on this subject and others. If you have a question or concern, please call our office to discuss it in detail. We want you to be comfortable with the agreement.
</p>
<h2>Have a question? CALL US AT 480-947-6500.</h2>
</div></div>
<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ�s in Arizona,
 Birthday party DJ�s in Arizona, Anniversary DJ�s in Arizona, Company event DJ�s Arizona, Corporate
 event DJ�s in Arizona, & private party DJ�s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright � 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>


</body>

</html>