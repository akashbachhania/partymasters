<html>
<head>
<title>Administration | Edit</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="admin.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="header">
<h1>Administration | Edit Pages</h1>
<ul>
<li><a id="active" href="">Edit Pages</a></li>
<li><a href="">Gallery</a></li>
<li><a href="">Slideshows</a></li>
<li><a href="">SEO</a></li>
<li><a href="">Traffic</a></li>
<li><a id="log" href="">Logout</a></li>
</div>
<center>


<div class="intro">
<br>
<img style="margin-left:-40%;width:15%;position:absolute;" src="logo.png"/>
<h3>Edit pages</h3>
<h5>Edit your pages content styling and design.</h5>
<br>
<ul>
<li><a href="">Unique Pages</a></li>
<li><a href="">Services Pages</a></li>
<li><a href="">Events Pages</a></li>
</ul>
</div>
<div class="pageslist">

<ul>
<li><a href="edit.php?id=home"><i class="fa fa-pencil-square-o"></i> Home</a></li>
<li><a href="edit.php?id=about"><i class="fa fa-pencil-square-o"></i> About us</a></li>
<li><a href="edit.php?id=pricequote"><i class="fa fa-pencil-square-o"></i> Price Quote</a></li>
<li><a href="edit.php?id=quote"><i class="fa fa-pencil-square-o"></i> Quote Form</a></li>
<li><a href="edit.php?id=contact"><i class="fa fa-pencil-square-o"></i> Contact us</a></li>
<li><a href="edit.php?id=reviews"><i class="fa fa-pencil-square-o"></i> Reviews</a></li>
<li><a href="edit.php?id=employment"><i class="fa fa-pencil-square-o"></i> Employment</a></li>
<li><a href="edit.php?id=faqs"><i class="fa fa-pencil-square-o"></i> Faqs</a></li>
</ul>

</div>
</center>
<div class="footer">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>

</body>
</html>