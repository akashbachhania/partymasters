<html>
<head>
<title>Price Quote | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<script src="jqueryslide.js"></script>
<script src="pmaz.js"></script>
<script src="slidecontrol.js"></script>
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>

	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>

</head>

<body>

<header>

<div class="header2">

<img id="navback" src="headerback.png"/>



<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>

<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>

</center>

</div>

</div>

<img id="logotop" src="logo.png"/>

</header>

<div id="container">
	<ul>
      	<li><img src="banner.png" width="100%" /><h5>sub caption</h5></li>
      	<li><img src="banner1.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner2.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner3.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner4.jpg" width="100%" /><h5>sub caption</h5></li>

      </ul>
<h1>Free Price Quote</h1>	  
      <span class="button prevButton"></span>
      <span class="button nextButton"></span>
</div>
<center>
</div>
<div class="leftbar">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>
</div>
<div class="rightside">

<div class="addbooth">
<h1>Memorable Events</h1>
<p>Party Masters provides all the entainment services you need for unforgettable events!</p>
<h2>More Services = More Savings!</h2>

<h3>Party Masters Services</h3>
<ul>
<li style="background-color:#22B14C;"><img src="iconset/booth.png"/> Photo Booths</li>
<li style="background-color:#B200FF;"><img src="iconset/dj.png"/> Disc Jockeys</li>
<li style="background-color:#29A2D2;"><img src="iconset/camera.png"/> Photographers</li>
<li style="background-color:#FF7F27;"><img src="iconset/led.png"/> LED Lighting</li>
<li style="background-color:#7F0000;"><img src="iconset/montage.png"/> Photo Montage</li>
<li style="background-color:#FF006E;"><img src="iconset/video.png"/> Videographers</li>
<li style="background-color:#002F86;"><img src="iconset/party.png"/> Event Rentals</li>
<li style="background-color:#002F86;"><img src="iconset/dance.png"/> Dance Floors</li>
<li style="background-color:#002F86;"><img src="iconset/projectors.png"/> Screens Projectors</li>
<li style="background-color:#002F86;"><img src="iconset/partyplan.png"/> Party Planning</li>

</ul>
<img src="logo.png"/>
<h5>Anything you need,<br>Just ask the Party Masters!</h5>
<a id="number" href="tel:1-480-947-6500">(480) 947-6500</a>
</div>
</div>
<div class="whitecenter">



<h1 id="cntnt">Free Price Quote Form</h1>
<p id="cntnt">Please fill out the form to receive a free price quote<br> The information You provide will help us further to understand your needs</p>
<br><br>
<form id="fullquote">
<label>Name *</label>
<input name="" type="text" placeholder="" required/><br>
<label>Email *</label>
<input name="" type="text" placeholder="" required/><br>
<label>Phone *</label>
<input name="" type="text" placeholder="" required/><br>
<label>Address *</label>
<input name="" type="text" placeholder="" required/><br>
<label>City, State, Zip *</label>
<input name="" type="text" placeholder="" required/><br>
<label>Event Type *</label>
<select>
  <option value="" selected>Select Your Event Type</option>
  <option value="">Weddings</option>
  <option value="">Birthdays</option>
  <option value="">Anniversairies</option>
  <option value="">Mitzvahs</option>  
  <option value="">Company/Corporate</option>
  <option value="">Casino Games</option>  
  <option value="">School Events</option>
  <option value="">Reunions</option>  
  <option value="">Super Sweet 16</option>
  <option value="">College Events</option>  
  <option value="">Kids Parties</option>
  <option value="">Private Parties</option>  
  <option value="">Weddings</option>
  <option value="">Retirement</option>
  <option value="">Bridal Showers</option>
  <option value="">Grand Openings</option>  
  <option value="">Theme Parties</option>
  <option value="">House Parties</option>  
  <option value="">Holiday Parties</option>
  <option value="">Fundraisers</option>  
  <option value="">Karaoke</option>
  <option value="">Other Events</option>    
</select><br>
<label>Event Location</label>
<select>
<option selected>Please Make Selection</option><option>NOT SURE YET (Still deciding)</option><option>OUR HOME</option><option></option><option>-- or, Select a Location Below --</option><option>Amara Resort</option><option>American Italian Club</option><option>American Legion #27</option><option>AmPac Tire Dist. Warehouse</option><option>Ancala Country Club</option><option>Anthem Golf and Country Club</option><option>Antique Wedding House</option><option>Arcadia Farms</option><option>Archstone Rio Salado Apartments</option><option>Arizona Biltmore Resort</option><option>Arizona Broadway Theatre</option><option>Arizona Country Club</option><option>Arizona Golf Resort & Conference Center</option><option>Arizona Grand Resort</option><option>Arrowhead Country Club</option><option>Ashley Manor</option><option>ASU Arboretum</option><option>ASU Rec Center</option><option>Aunt Chiladas</option><option>AZ Historical Society - Tempe</option><option>Bell Rock Inn and Suites</option><option>Bellair Country Club</option><option>Bentley Projects</option><option>Best Western Dobson Ranch Inn</option><option>Big Surf</option><option>Blackstone Country Club</option><option>Bobby-Q</option><option>Boojum Tree</option><option>Boulders Resort and Spa</option><option>Brittle Bush Bar & Grill</option><option>Camelback Inn, Marriott</option><option>Carefree Resort and Villas</option><option>Chaparral Suites Resort</option><option>Chart House Restaurant</option><option>Chiaroscuro Art Gallery</option><option>Cliff Castle Casino</option><option>Colina Elementary School</option><option>Congregation Beth Israel</option><option>Cooperstown</option><option>Copperwynd Resort & Spa</option><option>Corona Ranch</option><option>Cottonwood Country Club</option><option>Country Club at DC Ranch</option><option>Country Meadows Golf Club</option><option>Courtyard by Marriot</option><option>Cracker Jax - Scottsdale</option><option>Crowne Plaza North</option><option>Dave and Buster's - Desert Ridge</option><option>Dave and Buster's - Tempe Marketplace</option><option>Days Inn @ Scottsdale Fashion Square</option><option>DC Ranch Homestead</option><option>DC Ranch Water Park</option><option>Desert Botanical Gardens</option><option>Desert Breeze Park</option><option>Desert Mountain</option><option>Desert Shadows Elementry School</option><option>Diamond Canyon School Gym (Anthem)</option><option>Dillons Resturant & Catering</option><option>Dobson Ranch Inn</option><option>Don and Charlie's Restaurant</option><option>Doubletree Guest Suites - Phoenix</option><option>Doubletree Paradise Valley Resort- Scottsdale</option><option>E-Cadamie High School</option><option>Eagle Mountain</option><option>El Chorro Lodge</option><option>El Modena High School</option><option>Elks Lodge 335</option><option>Embassy Suite at Stonecreek</option><option>Embassy Suites @ The Biltmore</option><option>Embassy Suites Hotel - Phoenix/Scottsdale</option><option>Embassy Suites - Phoenix North</option><option>Embassy Suites - Tempe</option><option>Epicurean Events</option><option>Estrella Golf Course</option><option>Estrella Mountain Ranch</option><option>Exelencia School</option><option>Falcon Dunes</option><option>Falcon Golf Course</option><option>Fiesta Inn</option><option>FireSky Resort and Spa</option><option>Foothills Golf Club</option><option>Fountain Hills Park</option><option>Fountain Hills Community Center</option><option>Four Points Sheraton Hotel</option><option>Four Seasons @ Troon North</option><option>Freestone Park</option><option>Gainey Ranch Golf Club</option><option>Gainey Ranch Hyatt Regency</option><option>Gainey Suites Hotel</option><option>Garcia's Mexican Restaurant</option><option>Glendale Civic Center</option><option>Gold Canyon Golf Resort</option><option>Grace Inn</option><option>Grandview Steakhouse</option><option>Grayhawk Country Club</option><option>Gym at Desert Cross Church - Tempe</option><option>Hard Rock Cafe - Phoenix</option><option>Hard Rock Hotel - San Diego</option><option>Hatton Hall</option><option>Hawthorne Suites, Ltd.</option><option>Heard Museum</option><option>Herberger Theater</option><option>Hermosa Inn - Scottsdale</option><option>Hillcrest Golf Club</option><option>Hillton - PHX Airport</option><option>Hilton Garden Inn</option><option>Hilton of Scottsdale</option><option>Hilton Sedona Golf Resort & Spa</option><option>Holiday Inn Hotel & Suites</option><option>Holliday Inn Express</option><option>Holy Spirit Catholic Church</option><option>Hotel San Carlos</option><option>Hotel Valley Ho</option><option>House of Tricks Restaurant</option><option>Howard Johnsons</option><option>Hyatt Regency Scottsdale Resort</option><option>Ironworks @ The Bel Air Golf Course</option><option>Islands Community Center</option><option>Jacksons on 3rd</option><option>Kerr Cultural Center</option><option>Kilted Cat</option><option>L'Auberge De Sedona</option><option>La Mariposa</option><option>Lakeview Recreation Center</option><option>Landmark Restaurant</option><option>Las Brisas</option><option>Las Vegas Club Hotel & Casino</option><option>Legend Trail Community Center</option><option>Luke Air Force Base Officers Club</option><option>Marco Polo Supper Club</option><option>Maricopa County Fair Grounds</option><option>Marriot - Buttes</option><option>Marriott at McDowell Mountain</option><option>Marriott Desert Ridge</option><option>McCormick Ranch Golf Club</option><option>Mesa Convention Center</option><option>Mesa Hilton</option><option>Mesa Las Palmas</option><option>Millennium Resort</option><option>Mirabel Country Club</option><option>Montelucia - PV</option><option>Monterra</option><option>Moon Valley Country Club</option><option>Mountain Preserve</option><option>Nardini Manor</option><option>Nordstrom - Chandler</option><option>Nordstrom - Scottsdale</option><option>Nutrioso Arizona</option><option>Oakwood Country Club</option><option>Ocotillo Golf Resort</option><option>Old Main</option><option>Old Town Wedding & Event Center</option><option>Orange Tree Golf Resort</option><option>Overtime Sports Grille</option><option>P.F. Changs</option><option>Palm Valley Community Center</option><option>Palm Valley Golf Club</option><option>Pebble Creek Eagle's Nest Country Club</option><option>Phoenician Hotel & Resort</option><option>Phoenix Zoo</option><option>Phoenix Convention Center</option><option>Pioneer Arizona Living History Museum</option><option>Point of View - Hilton</option><option>Pointe Hilton Tapatio Cliffs</option><option>Power Ranch</option><option>Prescott Golf & Country Club</option><option>Prime Hotel</option><option>PRIVATE HOME</option><option>Paradise Valley Country Club</option><option>Quality Inn & Suites - Prescott</option><option>Raddison Resort & Spa</option><option>Radisson Hotel (Phoenix Airport)</option><option>Ramada Inn Phoenix North</option><option>Red Mountain Ranch</option><option>Regency Garden</option><option>Resort Suites Of Scottsdale</option><option>RIO Vista Park</option><option>Ritz Carlton Hotel - Phoenix</option><option>Royal Palms Resort & Spa</option><option>Rustler's Rooste</option><option>San Marcos Golf Resort & Conference Center</option><option>Sanctuary Golf Course</option><option>Sanctuary Resort</option><option>Satisfied Frog - Cave Creek</option><option>Scottsdale Air Center</option><option>Scottsdale Conference Resort</option><option>Scottsdale Culinary Institute</option><option>Scottsdale Plaza Resort</option><option>Scottsdale Ranch Clubhouse</option><option>Secret Garden</option><option>Sedona Golf Resort</option><option>Seven Canyons Resort - Sedona</option><option>Seville Golf & Country Club</option><option>Sheraton Cresent Hotel</option><option>Sheraton Four Points Metro Center</option><option>Sheraton Phoenix Airport Hotel</option><option>Sheraton Wild Horse Pass</option><option>South Mountain Park</option><option>Squaw Peak Hilton</option><option>St. Bernards church</option><option>St. George''s Fellowship Hall</option><option>St. James Parish Family</option><option>Stardust Dance Studio</option><option>Starfire Golf Resort-Scottsdale CC</option><option>Steele Indian School Park</option><option>Stone Creek Golf Club</option><option>Stonebridge Manor</option><option>Stoudemire's Restaurant - Phoenix</option><option>Sun Lakes Country Club - Cottonwood Club</option><option>SunBird Golf Resort</option><option>Sundance Community Center</option><option>Sunset Bowl</option><option>Superstition Springs Golf Club</option><option>Talking Stick Golf Club</option><option>Tempe Center for the Arts</option><option>Tempe Kiwanis Park Wave Pool</option><option>Tempe Mission Palms</option><option>Tempe Preparatory Academy</option><option>Temple Chai</option><option>Temple Emanuel</option><option>Terrace Green</option><option>The Barn at Power Ranch</option><option>The Castle at Ashley Manor</option><option>The Cedars</option><option>The Club @ Seven Canyons</option><option>The Farm At South Mountain</option><option>The Princess Resort</option><option>The Ranch House Restaurant</option><option>The Raven Golf Club @ Verrado</option><option>The Scottsdale Renaissance</option><option>The Sets</option><option>The Venue of Scottsdale</option><option>The Waterfront</option><option>The Wright House</option><option>The Wrigley Mansion</option><option>TPC of Scottsdale</option><option>Trilogy at Vistancia</option><option>Trilogy Golf Course</option><option>Troon Golf & Country Club</option><option>Troon North Golf Club</option><option>Tumbleweed Park</option><option>Tumbleweed Recreation Center</option><option>Tuscany Falls Country Club</option><option>University Club Of Phoenix</option><option>University of Phoenix Stadium (Cardinals Stadium)</option><option>Val Vista Lakes Banquet Facility</option><option>Valle Luna</option><option>Verrado</option><option>Villa Siena</option><option>Villa Tuscana</option><option>Village Club & Health Spas - DC Ranch</option><option>W Hotel - Scottsdale</option><option>Western Skies Golf Club</option><option>Westin Kierland Resort & Spa</option><option>Whitney's Circle Bar Ranch</option><option>Wigwam Resort</option><option>Wild Horse Pass Resort</option><option>Willow Canyon High School</option><option>Windemere Hotel and Conference Center</option><option>Windgate Ranch</option><option>Windstar Gardens</option><option>Wyndham Buttes Resort</option><option>LOCATION NOT ON THIS LIST</option><option value="Other">Other</option></select><br>
<label>Number Of Guests</label>
<input name="" type="email" placeholder=""/><br>
<label>Guest of Honor(s) name</label>
<input name="" type="text" placeholder=""/><br>
<label>Check All services you need:</label><br><br>
<input id="check" type="checkbox" name="Photo Booths" value="Photo Booths"> Photo Booths
<input id="check" type="checkbox" name="Disc Jockeys" value="Disc Jockeys"> Disc Jockeys
<input id="check" type="checkbox" name="Photographers" value="Photographers"> Photographers<br>
<input id="check" type="checkbox" name="Disc Jockeys" value="Disc Jockeys"> LED Lighting
<input id="check" type="checkbox" name="Photo Booths" value="Photo Booths"> Photo Montage
<input id="check" type="checkbox" name="Disc Jockeys" value="Disc Jockeys"> Videographers<br>
<input id="check" type="checkbox" name="Photo Booths" value="Photo Booths"> Event Rentals
<input id="check" type="checkbox" name="Disc Jockeys" value="Disc Jockeys"> Dance Floors
<input id="check" type="checkbox" name="Photo Booths" value="Photo Booths"> Screens Projectors<br>
<input id="check" type="checkbox" name="Disc Jockeys" value="Disc Jockeys"> Party Planning<br>
<label>Event Date</label>
<input id="date" name="date" type="date" placeholder="Name" required/><br>
<label id="centered">Event Time:</label>
<br><br>
from
<input id="time" name="" type="text" placeholder="00:00 am/pm"/>
To
<input id="time" name="" type="text" placeholder="00:00 am/pm"/><br>
<label>Message</label><br><br>
<textarea placeholder="Comments / Message / Questions"></textarea><br><br>
<label>How were you referred?</label>
<select>
<option>Please Make Selection</option><option>Arizona Weddings Magazine </option><option>A Party Masters' Event (family member)</option><option>A Party Masters' Event (as a guest)</option><option>A Party Masters' staff member</option><option>A friend referred me</option><option>Repeat customer</option><option>Another company referred me</option><option>Resort / Hotel</option><option>Catering Manager at a Resort/Hotel</option><option>GOOGLE - Search Engine</option><option>YAHOO! - Search Engine</option><option>BING - Search Engine</option><option>YELP! - online review directory</option><option>Facebook </option><option>Yellow Pages / Phone book (DEX)</option><option>DiscJockeys.com - Online Directory</option><option>MyPartyPlanner.com - Online Directory</option><option>PartyBlast.com - Online Directory</option><option>PartyPop.com - Online Directory</option><option>WeDJ.com - Online Directory</option><option>The Knot - Online Directory</option><option>PartyPlannerUSA.com - Online Directory</option><option>WeddingPlanningDirectory.com - Online Directory</option><option>ThePartyNetwork.com - Online Directory</option><option>Eventective.com - Online Directory</option><option>Decidio.com - Online Directory</option><option>DEXonline.com - Online Directory</option><option>YellowPages.com - Online Directory</option><option>Another Online Directory (not listed)</option><option>iAd (smart phone ad)</option><option>Photographer</option><option>Videographer</option><option>Florist</option><option>Party/Event planner</option><option>Bridal Show</option><option>TV commercial</option><option>TV Show</option><option>Radio advertisement</option><option>Newspaper article</option><option value="Other">Other</option>
</select><br>
<label>Please Enter Captcha</label>
<input id="cap" name="" type="text" placeholder="Captcha Code" required/><br>
<input id="submit" type="submit" value="Quote My Event"/>
</form>
</div>
<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>
</body>
</html>