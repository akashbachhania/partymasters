<html>
<head>
<title>Administration | Edit</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="admin.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="text.js"></script>
</head>

<body>
<div class="header">
<h1>Administration | Edit Pages</h1>
<ul>
<li><a id="active" href="">Edit Pages</a></li>
<li><a href="">Gallery</a></li>
<li><a href="">Slideshows</a></li>
<li><a href="">SEO</a></li>
<li><a href="">Traffic</a></li>
<li><a id="log" href="">Logout</a></li>
</div>
<center>


<div class="intro">
<br>
<img style="margin-left:-40%;width:15%;position:absolute;" src="logo.png"/>
<h3>Edit pages</h3>
<h5>Edit your pages content styling and design.</h5>
<br>

</div>
<div class="editcontent">
<form method="post" action="edithome.php">
<textarea name="home1">Speak to a Professional Entertainment Specialist now: <a href="tel:1-480-947-6500" style="color:#29A2D2;">(480) 947-6500<a></span></textarea><br>
<textarea name="home2"><span style="color:white;"> We provide </span>ALL<span style="color:white;"> the Entertainment Services you need for</span> UNFORGETTABLE<span style="color:white;"> Events and Parties!</span></textarea><br>
<textarea name="home3">Welcome to Party Masters Events + Services</textarea><br>
<textarea name="home4"><span style="color:#29A2D2;font-size:18px;">Party Masters Entertainment</span> 
provides professional on-location mobile Disc Jockey, Videography, Photography, Photo booth 
Rentals, Video booth rentals with Green Screen, Photo Montage – Slide Show services (PixVid™), Party Rentals & Party Planning 
services for any occasion throughout Arizona. We also offer specialty items such as: LED uplighting effects, Photo Booths, 
Green screen, magicians, jazz bands, and a full array of LED up-lighting effects for any occasion.<br><br> 

<span style="color:#D72323;font-size:18px;">Our Phoenix DJ service and Phoenix Party Planners,</span>
 made up of professional entertainers and experienced party planners, have thousands of events under 
our belts, for over 25 years. Plus, our AWARD WINNING First-Class customer service, for on-site party planning and party rental
 services, has helped us to become the most referred entertainment and party planning and party rental service company among 
 resorts, party planners, wedding planners, consultants, and previous customers.<br><br> 
<span style="color:#6DDC48;font-size:18px;">Get a FREE price quote</span> 
and check your event date availability and schedule your FREE No-Obligation personal consultation meeting 
with one of our in-house entertainment sales team members.</textarea><br>
<textarea name="home5">We Make Memories Happen</textarea><br>
<textarea name="home6">We understand that your special event is a once-in-a-lifetime celebration—whether it’s your wedding day,
anniversary, a monumental birthday celebration or corporate event. When you choose Party Masters Entertainment
to entertain at your event,or help plan your event, you are choosing a partner to help make your event 
as memorable as possible. </textarea><br>
<textarea name="home7">Professional</textarea><br>
<textarea name="home8">Our Arizona DJ’s pride themselves on being professional and working closely with other vendors at your event. Each local Arizona DJ & Videographer hired is “PME Certified”, meaning our wedding DJ’s and DJ’s for all services have the talent, experience, professionalism, passion, music knowledge,
 to meet our high standards at Party Masters Entertainment.</textarea><br>
<textarea name="home9">Affordable Pricing</textarea><br>
<textarea name="home10">Party Masters’ Phoenix DJ service has the most competitive and attractive prices in Arizona. 
Hire us for multiple event planning services in Phoenix... <a href="">Show More</textarea><br>
<textarea name="home11">Party Masters Events + Services</textarea><br>
<textarea name="home12">Voted Arizona's best! </textarea><br>
<textarea name="home13">A Memorable Gallery - Awesome stuff!</textarea><br>
<input type="file" name="pic" accept="image/*">
<br>
<input type="submit" name="submit" value="update">
</form>
</div>
</center>
<div class="footer">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>

</body>
</html>