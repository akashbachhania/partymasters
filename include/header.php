<header>
<div class="header2">
    <img id="navback" src="headerback.png"/>
    <center>
        <nav>
            <ul id="navbar">
                <li><a id="home" href="index.php">Home</a></li>
                <li><a href="about.php">About Us</a></li>
                <li><a href="pricequote.php">Price Quote</a></li>
                <li><a href="gallery.php">Photo Gallery</a></li>
                <li><a href="reviews.php">Reviews</a></li>
                <li><a href="employment.php">Employment</a></li>
                <li><a href="faq.php">Faqs</a></li>
                <li><a href="contact.php">Contact US</a></li>
            </ul>

            <a id="Menu" style="cursor: pointer;" onclick="ShowNav();"><i class="fa fa-bars"></i> Menu</a>
            <a id="HideNav" style="cursor: pointer;" onclick="HideNav();"><i class="fa fa-bars"></i> Menu</a>

            <div id="SmallNav">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About Us</a></li>
                    <li><a href="pricequote.php">Price Quote</a></li>
                    <li><a href="gallery.php">Photo Gallery</a></li>
                    <li><a href="reviews.php">Reviews</a></li>
                    <li><a href="employment.php">Employment</a></li>
                    <li><a href="faq.php">Faqs</a></li>
                    <li><a href="contact.php">Contact US</a></li>
                </ul>
            </div>
        </nav>
        
        <a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" src="logoscroll.png"/></a>

        <div class="logintop">
            <a id="log" href="#">
                <i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login
            </a>
        </div>
    </center>
</div>

<img id="logotop" src="logo.png"/>
</header>