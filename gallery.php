<html>

<head>
<title>Gallery | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
    <script src="slider.js" type="text/javascript"></script>
	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>
     <script type="text/javascript" language="javascript">
         $(function () {
             var $win = $(window);

             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                     document.getElementById('navback').style.opacity = 0.7;    
             });
			 
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logotop').style.opacity = 1;
			 });
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logoscroll').style.opacity = 0;
			 });
         });
    </script>
</head>

<body>

<div class="header2" style="margin-top:0px;">
<img id="navback" src="headerback.png"/>
<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>
<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" style="opacity:1;" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>
</center>

</div>

</div>
<script type="text/javascript">
window.onscroll = function (e){
document.getElementById('navback').style.opacity = 1;
document.getElementById('logotop').style.opacity = 0;
document.getElementById('logoscroll').style.opacity = 1;
} 
</script>


<center>


<script type="text/javascript">
(function(){
var i = 1;
var start = true;   
    setInterval(function(){
        if (i>0&&start){
         var o = $("#light").css("opacity");
        
            var s = parseFloat(o) - 0.6;
       
            $("#light").css("opacity", s.toString());
           i = s;    
        }
    
        else {
        start = false;
         var o = $("#light").css("opacity");
        var s = parseFloat(o) + 0.6;
       
         $("#light").css("opacity", s.toString());
        i = s; 
            if(s > 1) start = true;
        
        
        }
        
   
   
   
    }, 200);

 
})();

</script>
</div>
<div class="gallerypage">

<div class="galleryitems">
<h1>Party Masters Gallery</h1>
<h2>Making Memories Happen</h2>
</div>

<div class="galleryitems">
<ul>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>Weddings</h3>
<h5>63 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>Photography</h3>
<h5>153 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>Bar/Bat Mitzvah</h3>
<h5>66 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>Photo Montage</h3>
<h5>12 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>DiscJockeys</h3>
<h5>68 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>Decoration</h3>
<h5>116 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>Casino Games</h3>
<h5>27 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>Video Dance</h3>
<h5>34 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
<li>
<a href="gallery_weddings.php"><div class="gal_box">
<img id="top" src="banner.png">
<img id="middle" src="banner1.jpg">
<img id="bottom" src="banner2.jpg">
<h3>Kids Parties</h3>
<h5>30 Photos</h5>
<i class="fa fa-search-plus"></i>
</div></a>
</li>
</ul>

</div>
</div>
<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>


</body>

</html>