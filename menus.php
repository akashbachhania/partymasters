<h3>Services Pages</h3>
<ul>
<li><a href="edit.php?id=booth"><i class="fa fa-pencil-square-o"></i> Photo Booth</a></li>
<li><a href="edit.php?id=dj"><i class="fa fa-pencil-square-o"></i> Disc Jockeys</a></li>
<li><a href="edit.php?id=photographers"><i class="fa fa-pencil-square-o"></i> Photographers</a></li>
<li><a href="edit.php?id=led"><i class="fa fa-pencil-square-o"></i> Led Lighting</a></li>
<li><a href="edit.php?id=montage"><i class="fa fa-pencil-square-o"></i> Photo Montage</a></li>
<li><a href="edit.php?id=vid"><i class="fa fa-pencil-square-o"></i> Videographers</a></li>
<li><a href="edit.php?id=rentals"><i class="fa fa-pencil-square-o"></i> Event Rentals</a></li>
<li><a href="edit.php?id=dance"><i class="fa fa-pencil-square-o"></i> Dance Floors</a></li>
<li><a href="edit.php?id=projectors"><i class="fa fa-pencil-square-o"></i> Screen Projectors</a></li>
<li><a href="edit.php?id=planning"><i class="fa fa-pencil-square-o"></i> Party Planning</a></li>
</ul>
</div>
<div class="pagesrightlist">
<h3>Events Pages</h3>
<ul>
<li><a href="weddings.php"><i class="fa fa-pencil-square-o"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-pencil-square-o"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-pencil-square-o"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-pencil-square-o"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-pencil-square-o"></i> Company/Corporate</a></li><br>
<li><a href="casino.php"><i class="fa fa-pencil-square-o"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-pencil-square-o"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-pencil-square-o"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-pencil-square-o"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-pencil-square-o"></i> Super Sweet 16</a></li><br>
<li><a href="college.php"><i class="fa fa-pencil-square-o"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-pencil-square-o"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-pencil-square-o"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-pencil-square-o"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-pencil-square-o"></i> Bridal Showers</a></li><br>
<li><a href="grandopenings.php"><i class="fa fa-pencil-square-o"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-pencil-square-o"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-pencil-square-o"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-pencil-square-o"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-pencil-square-o"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-pencil-square-o"></i> Karaoke</a></li><br>
<li><a href="other.php"><i class="fa fa-pencil-square-o"></i> Other Events</a></li>
</ul>