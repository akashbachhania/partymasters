<html>
<head>
<title>Party Planning | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<script src="jqueryslide.js"></script>
<script src="pmaz.js"></script>
<script src="slidecontrol.js"></script>
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>

	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>

</head>

<body>

<header>

<div class="header2">

<img id="navback" src="headerback.png"/>



<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>

<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>

</center>

</div>

</div>

<img id="logotop" src="logo.png"/>

</header>

<div id="container">
	<ul>
      	<li><img src="banner.png" width="100%" /><h5>sub caption</h5></li>
      	<li><img src="banner1.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner2.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner3.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner4.jpg" width="100%" /><h5>sub caption</h5></li>

      </ul>
<h1>Party Planning</h1>	  
      <span class="button prevButton"></span>
      <span class="button nextButton"></span>
</div>
<center>
</div>
<div class="leftbar">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>
</div>
<div class="whitesection">
<h1>Complete Party Planning</h1>
<p>Before you plan your next Arizona event, Party Masters can assist and provide you with the most extensive professional party and event planning for your Arizona Wedding, Birthday, Anniversary, Bar or Bat Mitzvah, corporate event, or any type of party celebration in Arizona. Just contact us and one of our professional party planners will sit down with you and provide you with all of the details regarding you event.
<br>
To get started, send us an email with your event date, event type, and services you require. One of our Professional party planners will contact you to set up a FREE No-Obligation in-person consultation at our office, or at a location of your choice. Click here to send us your information now or simply call us at 480-947-6500 to speak to one of our professional Arizona party planners. We look forward to assisting you to help create and plan your event without any guesswork or hassle.
<br>
We can provide you with all of your Arizona party & event planning needs by incorporating the following list of Party supplies & items for your next Arizona event:
</p>
<h1>Party Planning Items</h1>
<ul id="content">
<li>Backdrops & Drapes & Arches</li>
<li>Balloon Decorations (arches, etc)</li>
<li>Big Screen Video Projection (Front & Rear)</li>
<li>Bounce House inflatables (for Kids)</li>
<li>Casino Tables (Black Jack, Craps, Roulette)</li>
<li>Centerpieces (Custom colors)</li>
<li>Chairs, tables, & Sofas (Acrylic/Leather)</li>
<li>Chocolate Fountains</li>
<li>Cooling systems</li>
<li>Dance Floors (Standard & LED lighted floors)</li>
<li>Decorations & Balloons</li>
<li>Electricity (Portable Power)</li>
<li>Fire pits (Gas)</li>
<li>Furniture (Coffee, cocktail & End Tables)</li>
<li>Heaters & Heating systems</li>
<li>Haunted Houses, set designs, etc.</li>
<li>LED lighting effects & up-lighting (any color)</li>
<li>Lighted Tables (High Tops and Standard)</li>
<li>Linens & Tablecloths</li>
<li>Park Your Pool (custom platform over your pool)</li>
<li>Tents & Cabanas</li>
<li>Theme party sets (Custom designed and built)</li>
<li>Video Screens & Projectors</li>
</ul>

<h1>Party Planning Services Include</h1>
<ul id="content">
<li>Bartenders & Servers</li>
<li>Beverage Service Staff</li>
<li>Budgets</li>
<li>Catering & Buffets (Lunch, Dinner, Snacks, & Desserts)</li>
<li>Cleaning Staff (before, during, & after)</li>
<li>Consultations & Consulting</li>
<li>Coordination (on-site)</li>
<li>Customizing your event</li>
<li>DJ/MC Music & Entertainment</li>
<li>Decorating & Decor</li>
<li>Design & Layout ideas (tables, chairs, dance floor, lighting, etc.)</li>
<li>Food Service Staff</li>
<li>Gazebos & Cabanas</li>
<li>Installation & Setup</li>
<li>Lighting & Staging</li>
<li>Photo Booths (Green Screen & Video)</li>
<li>Photographers</li>
<li>PixVid™ (Photo/Picture – Video montage services)</li>
<li>Security & Security guards</li>
<li>Small Jobs / Large Jobs</li>
<li>Themes & Theme Parties</li>
<li>Videography</li>
</div>
<div class="selectdate">
<h1>Check Availability and get a Price Quote</h1>
<a href="pricequote.php">Get a Free Price Quote</a>

</div>



<center>
<div id="blackfade">
<div id="1">
<div id="1" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('1');"><i class="fa fa-times-circle-o"></i></a>
<img src="wedding.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(2);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(13);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="2">
<div id="2" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('2');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(3);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(1);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="3">
<div id="3" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('3');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner2.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(4);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(2);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="4">
<div id="4" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('4');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner3.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(5);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(3);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="5">
<div id="5" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('5');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner4.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(6);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(4);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="6">
<div id="6" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('6');"><i class="fa fa-times-circle-o"></i></a>
<img src="box1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(7);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(5);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="7">
<div id="7" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('7');"><i class="fa fa-times-circle-o"></i></a>
<img src="boxbanner.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(8);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(6);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="8">
<div id="8" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('8');"><i class="fa fa-times-circle-o"></i></a>
<img src="img35.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(9);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(7);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="9">
<div id="9" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('9');"><i class="fa fa-times-circle-o"></i></a>
<img src="memories.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(10);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(8);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="10">
<div id="10" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('10');"><i class="fa fa-times-circle-o"></i></a>
<img src="w1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(11);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(9);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="11">
<div id="11" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('11');"><i class="fa fa-times-circle-o"></i></a>
<img src="w1.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(12);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(10);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="12">
<div id="12" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('12');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(13);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(11);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="13">
<div id="13" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('13');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner4.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(1);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(12);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>


</div>
</center>
<div class="gallery">
<h1>Gallery Sample for testing</h1>
<div id="center" class="gallerylist">
<ul>
<li>
<img onclick="Showbox('1');"  class="small" src="wedding.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('2');" class="small" src="banner1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('3');"  class="small" src="banner2.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('4');"  class="small" src="banner3.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('5');"  class="small" src="banner4.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('6');"  class="small" src="box1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('7');"  class="small" src="boxbanner.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('8');"  class="small" src="img35.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('9');"  class="small" src="memories.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('10');" class="small" src="w1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('11');" class="small" src="banner.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('12');" class="small" src="w1.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('13');" class="small" src="banner4.png"><i class="fa fa-search-plus"></i>
</li>
</ul>
</div>


<center>

<a href="gallery.php">Checkout Party Master full Gallery</a>
</div>
</center>
<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>
</body>
</html>