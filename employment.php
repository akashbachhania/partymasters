<html>

<head>
<title>Employment | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
    <script src="slider.js" type="text/javascript"></script>
	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>
     <script type="text/javascript" language="javascript">
         $(function () {
             var $win = $(window);

             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                     document.getElementById('navback').style.opacity = 0.7;    
             });
			 
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logotop').style.opacity = 0.8;
			 });
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logoscroll').style.opacity = 0;
			 });
         });
    </script>
</head>

<body>


<div class="header2" style="margin-top:-50px;">
<img id="navback" src="headerback.png"/>
<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>
<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" style="opacity:1;" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>
</center>

</div>

</div>

<center>
<script type="text/javascript">
window.onscroll = function (e){
document.getElementById('navback').style.opacity = 1;
document.getElementById('logotop').style.opacity = 0;
document.getElementById('logoscroll').style.opacity = 1;
} 
</script>
<script type="text/javascript">
(function(){
var i = 1;
var start = true;   
    setInterval(function(){
        if (i>0&&start){
         var o = $("#light").css("opacity");
        
            var s = parseFloat(o) - 0.6;
       
            $("#light").css("opacity", s.toString());
           i = s;    
        }
    
        else {
        start = false;
         var o = $("#light").css("opacity");
        var s = parseFloat(o) + 0.6;
       
         $("#light").css("opacity", s.toString());
        i = s; 
            if(s > 1) start = true;
        
        
        }
        
   
   
   
    }, 200);

 
})();

</script>

<div class="leftbar">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>
</div>
<div class="rightside">

<div class="addbooth">
<h1>Memorable Events</h1>
<p>Party Masters provides all the entainment services you need for unforgettable events!</p>
<h2>More Services = More Savings!</h2>

<h3>Party Masters Services</h3>
<ul>
<li style="background-color:#22B14C;"><img src="iconset/booth.png"/> Photo Booths</li>
<li style="background-color:#B200FF;"><img src="iconset/dj.png"/> Disc Jockeys</li>
<li style="background-color:#29A2D2;"><img src="iconset/camera.png"/> Photographers</li>
<li style="background-color:#FF7F27;"><img src="iconset/led.png"/> LED Lighting</li>
<li style="background-color:#7F0000;"><img src="iconset/montage.png"/> Photo Montage</li>
<li style="background-color:#FF006E;"><img src="iconset/video.png"/> Videographers</li>
<li style="background-color:#002F86;"><img src="iconset/party.png"/> Event Rentals</li>
<li style="background-color:#002F86;"><img src="iconset/dance.png"/> Dance Floors</li>
<li style="background-color:#002F86;"><img src="iconset/projectors.png"/> Screens Projectors</li>
<li style="background-color:#002F86;"><img src="iconset/partyplan.png"/> Party Planning</li>

</ul>
<img src="logo.png"/>

<a id="number" href="tel:1-480-947-6500">(480) 947-6500</a>
</div>
</div>
<div class="whitecenter">
<div class="employment">
<h1>Party Marters Employment</h1>
<h4>I wanna be a Party Master!</h4>
<p id="top">If you have an outgoing personality and are not fazed by being in front of a crowd, love a variety of 
music, and available to work on the weekends, then you should try becoming a mobile Disc Jockey, 
Photographer, Videographer, and more with Party Masters Entertainment.
We encourage you to contact us if you are seeking for a career in entertainment services. 
Party Masters Entertainment offers FREE training to anyone that aspires to learn more about the 
entertainment service business.<br>
This job is part-time and you are considered an independent contractor. 
You will need to be available on the weekends. Most of the jobs (95%), land on the weekends 
(mostly Saturday afternoon and into the evenings). It’s a great way to supplement your full-time job or just earn some extra cash. Best of all, it’s rewarding and fun!
Already an experienced DJ? We will consider hiring you if you have the music knowledge, 
experience, professionalism, communication skills and most important, personality.<br>
Are you a Videographer or Photographer looking for extra work? If so, we are always looking for 
professionals with experience. The more experience, means more money for you. Read more information below.
If you do not have any experience, we will train you. Please understand this will take some time and a 
lot of patience on your end. The education and information we give to you will be ultimately rewarding 
for the rest of your life.
BTW… any tips and over time you receive is 100% yours! We do not take a percentage of these earnings.</p>

<h3>You must meet the following criteria to become part of our team</h3>
<ul>
<li><p>Must be at least 21 years of age</p></li>
<li><p>Must have a valid drivers license.</p></li>
<li><p>Must have own transportation</p></li>
<li><p>Must be a legal US citizen.</p></li>
<li><p>Cannot have a criminal record.</p></li>
<li><p>Must be available on all weekends (Friday nights, all day/night Saturday, and some Sundays).</p>
<li><p>Knowledge of a variety of music (DJs).</p></li>
<li><p>Able to talk on a microphone (DJs).</p></li>
<li><p>Fearless of being in front of an audience.</p></li>
<li><p>Must be friendly, outgoing, professional, and punctual.</p></li>
</ul>

<h2>Photographers and/or Videographers!</h2>
<p id="second">If you have an eye for capturing the sights, sounds and emotions of a party, we have a job for you 
(well, maybe). If you can be available to work on the weekends, then you should consider working with us 
at Party Masters Entertainment.
We are always looking for professional, reliable talented people that can take photos or shoot video on 
the weekends at our events, for our clients. You must have you own equipment and it must be professional 
quality. No consumer equipment allowed.
Keep in mind… This job is part-time and you are considered an 
independent contractor. You will need to be available on the weekends. Most of the jobs (95%), 
land on the weekends (mostly Saturday afternoon and into the evenings). It’s a great way to supplement 
your full-time job or just earn some extra cash. Best of all, it’s rewarding and fun!
Already an experienced photographer and/or videographer? We will consider hiring you if you have the 
music knowledge, experience, professionalism, communication skills and most important, personality.
If you do not have any experience, we will train you. Please understand this will take some time and a 
lot of patience on your end. The education and information we give to you will be ultimately rewarding 
for the rest of your life.
BTW… any tips and over time you receive is 100% yours! We do not take a percentage of 
these earnings.</p>
<h3>You must meet the following criteria to become part of our team</h3>
<ul>
<li><p>Must be at least 21 years of age</p></li>
<li><p>Must have a valid drivers license.</p></li>
<li><p>Must have own transportation</p></li>
<li><p>Must be a legal US citizen.</p></li>
<li><p>Cannot have a criminal record.</p></li>
<li><p>Must be available on all weekends (Friday nights, all day/night Saturday, and some Sundays).</p></li>
<li><p>Knowledge of photography and/or Videography.</p></li>
<li><p>Know how to use PhotoShop and/or a video editing program.</p></li>
<li><p>Fearless of people you don’t know.</p></li>
<li><p>Must be friendly, outgoing, professional, and punctual.</p></li>
</ul>
<h5>To join our staff of professionals, please contact via Phone or send us an email.</h5>

<p id="bottom">*NOTE: If you are sending us an E-mail, please include your full name, phone number and the best time to contact you. Also, a brief description about you, plus your level of experience as a Photographer/Videographer at this time (beginner to professional).</p>
</div></div>

<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>


</body>

</html>