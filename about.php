<html>

<head>
    <?php include 'include/head.php';?>
    <link rel="stylesheet" href="pages.css" type="text/css" media="all" />
</head>

<body>

<div class="header2" style="margin-top:0px;">
<img id="navback" src="headerback.png"/>
<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>
<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" style="opacity:1;" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>
</center>

</div>

</div>



<center>


<script type="text/javascript">
(function(){
var i = 1;
var start = true;   
    setInterval(function(){
        if (i>0&&start){
         var o = $("#light").css("opacity");
        
            var s = parseFloat(o) - 0.6;
       
            $("#light").css("opacity", s.toString());
           i = s;    
        }
    
        else {
        start = false;
         var o = $("#light").css("opacity");
        var s = parseFloat(o) + 0.6;
       
         $("#light").css("opacity", s.toString());
        i = s; 
            if(s > 1) start = true;
        
        
        }
        
   
   
   
    }, 200);

 
})();

</script>
</div>
<br><br><br><br>
<div class="leftbar">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>
</div>
<div class="rightside">

<div class="addbooth">
<h1>Memorable Events</h1>
<p>Party Masters provides all the entainment services you need for unforgettable events!</p>
<h2>More Services = More Savings!</h2>

<h3>Party Masters Services</h3>
<ul>
<li style="background-color:#22B14C;"><img src="iconset/booth.png"/> Photo Booths</li>
<li style="background-color:#B200FF;"><img src="iconset/dj.png"/> Disc Jockeys</li>
<li style="background-color:#29A2D2;"><img src="iconset/camera.png"/> Photographers</li>
<li style="background-color:#FF7F27;"><img src="iconset/led.png"/> LED Lighting</li>
<li style="background-color:#7F0000;"><img src="iconset/montage.png"/> Photo Montage</li>
<li style="background-color:#FF006E;"><img src="iconset/video.png"/> Videographers</li>
<li style="background-color:#002F86;"><img src="iconset/party.png"/> Event Rentals</li>
<li style="background-color:#002F86;"><img src="iconset/dance.png"/> Dance Floors</li>
<li style="background-color:#002F86;"><img src="iconset/projectors.png"/> Screens Projectors</li>
<li style="background-color:#002F86;"><img src="iconset/partyplan.png"/> Party Planning</li>

</ul>
<img src="logo.png"/>

<a id="number" href="tel:1-480-947-6500">(480) 947-6500</a>
</div>
</div>

<div class="whitecenter">

<div class="aboutus">
<h1>About us </h1>
<p>Party Masters Entertainment LLC is locally owned and operated by Andrew Starr.
 Andrew was born on the East coast and originally from Philadelphia, where he arrived in Arizona
 to attend Arizona State University for broadcast communications. While at ASU, he began his
 mobile DJ work on the weekends where he performed at college parties and eventually expanded his
 talent to weddings and company & private functions.<br><br>Traveling the country and learning from others in the event and party planning industry, 
Andrew spend time in New York to learn about this great industry. 
He also learned a lot traveling to Miami, Seattle, Los Angeles, Nashville, Chicago, 
and many other cities. But, after being about to travel to New York city, since he was from 
Philadelphia, he was able to pick up details of corporate events in New York and in Philadelphia,
 from many other great event planning companies on the East coast.<br><br>

Andrew’s sales and entertainment staff have the most overall knowledge and experience when 
compared to other services. He and his party planning and event planning experts are very 
experienced and trained to provide great customer service prior to your event, until the last 
guest leaves your party. The best feeling is to know that everyone had a good time at your 
event and it might be something they talk about the next day and weeks/months after.</p>
</div>
<div class="philosophy">
<h1>The Party Masters Philosophy is Simple</h1>
<p>We do our best to provide the best customer service 
and entertainment for each and every client. Our goal is to make each 
event better than the last, and with our experience it has been a proven success. 
We hand pick experienced DJ entertainers and quality individuals to represent our 
clients and Party Masters Entertainment. For quality control, each entertainer, 
regardless of experience, must complete our training program to become a “PME Certified”™ 
entertainer. It’s very important the Party Masters name is represented with the best in the 
business. Honestly, we are not the cheapest, nor the most expensive. However, we are at the 
top of our game when it comes to customer service as well as our exceptional ability to 
comprehend the environment while entertaining your guests. As the old adage says: 
“You get what you pay for”, and we believe you will see the difference with Party Masters 
Entertainment.</p>
</div>
</div>


<div class="aboutandy">
<h1>About Andrew Starr</h1>
<img src="aboutandy.jpg"/>
<p>After arriving in Arizona, and beginning his DJ career, Andrew 
(along with business partner, Dave Rajput) developed and produced and on-air radio program 
named “Hot Mix”. Hot Mix was the first show of it’s kind to be aired on radio in Arizona. 
The program was creative and innovative, yet simple. Take the club/extended versions of popular 
hit songs and put them on the air in a radio friendly listening environment. Hot Mix was a 25 
minute non-stop continuous music segment with approximately 7 or 8 songs mixed beat-to-beat and 
back-to-back. To read more about Hot Mix, click here. After “Hot Mix”, in early 2002, Andrew 
branched out and created a worldwide DJ directory named DiscJockeys.com. Today, DiscJockeys.com 
is the largest entertainment directory online with over 10,000 DJ services listed! Check us out, 
we’re listed! In mid 2006, MyPartyPlanner.com was launched as a compliment and extension of 
DiscJockeys.com. Andrew also co-created and developed another online entertainment directory 
named PartyBlast.com. He is currently working on new sites, such as LimosUSA.com and others 
to help people in the US and Canada locate the best professional entertainment services for 
their event</p>
</div>
<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>


</body>

</html>