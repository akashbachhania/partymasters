<html>

<head>
<title>Reviews | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
    <script src="slider.js" type="text/javascript"></script>
	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>
     <script type="text/javascript" language="javascript">
         $(function () {
             var $win = $(window);

             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                     document.getElementById('navback').style.opacity = 0.7;    
             });
			 
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logotop').style.opacity = 1;
			 });
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logoscroll').style.opacity = 0;
			 });
         });
    </script>
</head>

<body>

<div class="header2" style="margin-top:0px;">
<img id="navback" src="headerback.png"/>
<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>
<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" style="opacity:1;" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>
</center>

</div>

</div>



<center>


<script type="text/javascript">
(function(){
var i = 1;
var start = true;   
    setInterval(function(){
        if (i>0&&start){
         var o = $("#light").css("opacity");
        
            var s = parseFloat(o) - 0.6;
       
            $("#light").css("opacity", s.toString());
           i = s;    
        }
    
        else {
        start = false;
         var o = $("#light").css("opacity");
        var s = parseFloat(o) + 0.6;
       
         $("#light").css("opacity", s.toString());
        i = s; 
            if(s > 1) start = true;
        
        
        }
        
   
   
   
    }, 200);

 
})();

</script>
</div>
<br><br><br><br>
<div class="leftbar">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>
</div>
<div class="rightside">

<div class="addbooth">
<h1>Memorable Events</h1>
<p>Party Masters provides all the entainment services you need for unforgettable events!</p>
<h2>More Services = More Savings!</h2>

<h3>Party Masters Services</h3>
<ul>
<li style="background-color:#22B14C;"><img src="iconset/booth.png"/> Photo Booths</li>
<li style="background-color:#B200FF;"><img src="iconset/dj.png"/> Disc Jockeys</li>
<li style="background-color:#29A2D2;"><img src="iconset/camera.png"/> Photographers</li>
<li style="background-color:#FF7F27;"><img src="iconset/led.png"/> LED Lighting</li>
<li style="background-color:#7F0000;"><img src="iconset/montage.png"/> Photo Montage</li>
<li style="background-color:#FF006E;"><img src="iconset/video.png"/> Videographers</li>
<li style="background-color:#002F86;"><img src="iconset/party.png"/> Event Rentals</li>
<li style="background-color:#002F86;"><img src="iconset/dance.png"/> Dance Floors</li>
<li style="background-color:#002F86;"><img src="iconset/projectors.png"/> Screens Projectors</li>
<li style="background-color:#002F86;"><img src="iconset/partyplan.png"/> Party Planning</li>

</ul>
<img src="logo.png"/>

<a id="number" href="tel:1-480-947-6500">(480) 947-6500</a>
</div>
</div>

<div class="whitecenter">
<div class="reviews">
<h1>Party Masters Customers' Reviews</h1>
<h2>Previous Customer reviews over the past year…</h2>
<p>
<i class="fa fa-quote-left"></i> 
Thank you so much for a GREAT wedding and reception! The music and atmosphere was amazing! We, and all 
our guests, had the most 
wonderful & fun night. So many people commented about how awesome you were. Many, Many thanks!
<i class="fa fa-quote-right"></i>
<span> -Ivan & Chelsey Nicholson</span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
Thank you so so much for being such a cool, calm, and amazing entertainer on our wedding night. We saw people dancing who usually never dance. You played all of our requests and favorite songs all night long, and songs we never thought of, but were perfect! We feel like we not only found a great company, but a company we will be proud of to refer to anyone for any occasion.
<i class="fa fa-quote-right"></i>
<span> -Jennie & Kyle</span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
Thank you again for all of your assistance and taking the time to help plan out our special day. The music & the DJ were wonderful! Party Masters made our wedding memorable!
<i class="fa fa-quote-right"></i>
<span> -Kelly & Carrie Lownds </span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
Thank you so much for making our party for Maya so special. Everyone was saying how nice everything was and how much they loved the DJ’s music. Your entire company helped me more than I expected.
<i class="fa fa-quote-right"></i>
<span> -Lisa, Mark & Maya</span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
Thanks again for making our wedding day such a fun and memorable time. You were great! You and your staff listened to our needs and requests to make the evening special.
<i class="fa fa-quote-right"></i>
<span> - Marily & Rick</span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
We used “Party Masters Entertainment” for our Wedding & Reception. It was THE BEST CHOICE… they were so professional and organized we did not have to worry about a thing ! From beginning to end … so many people were coming up to us and was asking “what is the name of the company you hired” they are incredible ! To this day we have several people say that, that was such a ” Hollywood Wedding” the dance floor was packed the whole night and we knew when all of the girls had their shoes off people was having a Great time !! What a success … just wish we could do it again !!!
<i class="fa fa-quote-right"></i>
<span> -Tom & Jeannine Sweeney</span>
</p>
<p>
<i class="fa fa-quote-left"></i>
Thank you for providing us with an amazing casino event for our executives at our corporate office in Scottsdale. I want to express our appreciation of Party Masters services during our fundraiser. Party Masters services is a professional and an efficient company that complimented our needs, allowing us to scale out event and design a custom package to entertain our employees. We hosted a successful fund raising event thanks to your services and support. We know that commitment to customer keeps our guests coming back and it meant a lot that your team delivered such care to our event. Thank you for your professionalism and all that you did to ensure our success. Our event was a great hit and one that will not be forgotten for months to come.
<i class="fa fa-quote-right"></i>
<span> -John Johnson, CIO P.F. Chang’s China Bistro, Inc.</span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
Hi. Just wanted to tell you that I thought you were great and so was everyone else you recommended for Karen’s party last Saturday. I will certainly recommend you to anyone that needs party planning, music entertainment and videography. Again, you were lots of fun, professional and easy to work with. Thanks so much!
<i class="fa fa-quote-right"></i>
<span> -Joan Vogel</span>
</p><p>
<i class="fa fa-quote-left"></i> 
I had no idea what I was getting myself into when I was researching entertainment for my wedding- long distance….
I’m so thankful I ran across Party Masters Entertainment and spoke to the owner Pinky Lieb. He was excellent- so professional and really listened to my expectations. I highly recommend Party Masters Entertainment to any bride looking for a really fun party atmosphere.
<i class="fa fa-quote-right"></i>
<span> -Yasmeen O. Wedding – from Arlington, VA</span>
</p><p>
<i class="fa fa-quote-left"></i> 
It was my 50th birthday and my wife and I wanted to throw a party that would be one to remember. We did not want to plan and run the entire event ourselves. We needed professional help, and help that would not break the bank. Party Masters Entertainment came highly recommended from a friend. After meeting with the owners, Andrew Starr and Pinky Lieb, we immediately knew we had the right place. They were able to assist us with planning a theme, and offered ideas that would make the party special, and WOW was it special. They were easy going, worked within our budget and were sensitive to our own ideas. They handled everything such as great DJ Music, a photo montage that everyone loved, professional photography and a video production to save the memories that we still watch to this day. We received many comments from people attending who said it was one of the best parties they had ever attended. I would highly recommend Party Masters to anyone wanting to throw a special party or event and want it to be something to remember for the rest of their lives.
<i class="fa fa-quote-right"></i>
<span> -Rory Hood, PV.</span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
Thank you so much for providing us with the music for our ASU tailgate party for the big game. The music made the environment festive – Even the caterer commented on how good it was! Thank again for all of your help!
<i class="fa fa-quote-right"></i>
<span> -Cindy Dick & Traci Nicksic / ASU Alumni</span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
Thank you again for making Melissa’s Bat Mitzvah day so incredibly special, I have never seen Melissa so happy – I truly mean this. From moment we met with you and your staff to the day of our event, the experience was extremely professional, friendly, responsive in our daughter’s preferences for her party. Everyone, including the adults, gathered around the red carpet you provided along with the four security guards leading Melissa to the dance floor. What a way to kick off the party! WOW! The music and light show was perfect all night long and you facilitated the candle lighting ceremony, speeches, and guest hora, perfectly! You also did a great job keeping the party moving and everyone dancing all night long. I didn’t see a child sit down all night (except for dinner). Great job! I hope this letter helps for any future endeavors. All the best!
<i class="fa fa-quote-right"></i>
<span> -Barbara Greenberg</span>
</p>
<p>
<i class="fa fa-quote-left"></i> 
Thank you so much for your participation in the Moon Valley Country Club Bridal Expo. Your music selection and videos made an outstanding impact on the attendees. Everyone enjoyed themselves.
<i class="fa fa-quote-right"></i>
<span> -Sara Seivert / Catering Director, Moon Valley CC</span>
</p>
<h5>If you’d like to see more testimonials/reviews from our customers, please ask. We have many other event reviews in Phoenix and pages of satisfied customers from weddings in phoenix, Mitzvahs in Tempe, Birthdays in Chandler, Anniversaries in Glendale, to high school proms in Mesa, to company and corporate events in Scottsdale, over the past years, and we are very proud to show them off. 
<br>Call us at 480-947-6500 to make an appointment to view them all.</h5>
</div>
</div>

<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>


</body>

</html>