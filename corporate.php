<html>

<head>
<title> Company/Corporate | Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<script src="jqueryslide.js"></script>
<script src="pmaz.js"></script>
<script src="slidecontrol.js"></script>
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<link rel="stylesheet" href="pages.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>

	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>

</head>

<body>

<header>

<div class="header2">

<img id="navback" src="headerback.png"/>



<center>
<nav>
<ul id="navbar">

<li><a id="home" href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>

</ul>
</nav>

<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>

</center>

</div>

</div>

<img id="logotop" src="logo.png"/>

</header>

<div id="container">
	<ul>
      	<li><img src="banner.png" width="100%" /><h5>sub caption</h5></li>
      	<li><img src="banner1.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner2.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner3.jpg" width="100%" /><h5>sub caption</h5></li>
		<li><img src="banner4.jpg" width="100%" /><h5>sub caption</h5></li>

      </ul>
<h1>Company / Corporate</h1>	  
      <span class="button prevButton"></span>
      <span class="button nextButton"></span>
</div>
<center>
</div>
<div class="rightside">

<div class="addbooth">
<h1>Memorable Events</h1>
<h2><span style="font-size:20px">Add</span> More Services<br>
Get More <span style="font-size:20px">Savings!</span></h2>


<ul>
<a href="photo_booth.php"><li style="background-color:#22B14C;"><img src="iconset/booth.png"/> Photo Booths</li></a>
<a href="discjockeys.php"><li style="background-color:#B200FF;"><img src="iconset/dj.png"/> Disc Jockeys</li></a>
<a href="photographers.php"><li style="background-color:#29A2D2;"><img src="iconset/camera.png"/> Photographers</li></a>
<a href="led_lighting.php"><li style="background-color:#FF7F27;"><img src="iconset/led.png"/> LED Lighting</li></a>
<a href="photo_montage.php"><li style="background-color:#D72323;"><img src="iconset/montage.png"/> Photo Montage</li></a>
<a href="videographers.php"><li style="background-color:#FF006E;"><img src="iconset/video.png"/> Videographers</li></a>
<a href="event_rentals.php"><li style="background-color:#002F86;"><img src="iconset/party.png"/> Event Rentals</li></a>
<a href="dance_floors.php"><li style="background-color:#002F86;"><img src="iconset/dance.png"/> Dance Floors</li></a>
<a href="screen_projectors.php"><li style="background-color:#002F86;"><img src="iconset/projectors.png"/> Screens Projectors</li></a>
<a href="party_planning.php"><li style="background-color:#002F86;"><img src="iconset/partyplan.png"/> Party Planning</li></a>

</ul>
<img id="serviceslogo" src="logo.png"/>

<a id="number" href="tel:1-480-947-6500">(480) 947-6500</a>
</div>
</div>
<div class="leftbar">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>
</div>
<div class="bluesection">



<div class="article"/>
<h2>Company/Corporate</h2>
<p>Party Masters Entertainment is your complete source for helping you create, design and plan your next company/corporate event. We can provide the ideas or facilitate your plans with our services: Music entertainment (DJ and/or MC host), LED Lighting, Photography, Videography, Photo Booths with Green Screen and Video messaging, Event Plannning, Party Rentals… even create/produce a professional Photo Montage slide shows (video production with photos) and set up a large video screen(s) with rear projection, for any event! Just let us know what you are looking for and we will make it happen, trouble free!
<br><br>
We will fully customize our services for any event or function your company/corporation is planning. Large, medium or small sized events, we will work closely with you and your planning team to meet your needs and within your budget. Maybe you only need a simple sound system setup or an LED lighting package. Just tell us what you need, and we can assist you to make your event a trouble-free success. Call us today for a FREE no-obligation consultation with one of our expert company/corporate planners at 480-947-6500
</p>
<h2>We can help with your next event…</h2>
<p>Conventions/Trade Shows/Expos
Grand Openings
Dinners/Luncheons
Holiday Parties
Fashion Shows
Company BBQs & Picnics
Dinner Dances
Employee Recognition Dinners
Sales/Marketing Seminars
Retirement Dinners
New Product Releases
Casino Parties
Game Shows
Client Parties
General Office</p>
<h2>Our Satisfied Clients:</h2>

<p>PF Changs / Pei Wei Corporate Office
GoDaddy
Arizona State University
Southwest Airlines
Waste Management – Phoenix Open
Nordstroms
APS
eBay/PayPal corporation
3TV on-air staff
12 News on-air staff
FOX Sports
NBA Special Events
NFL – Super Bowl and Pro Bowl Events (2015)
Arizona DiamondBacks
Phoenix Suns
Arizona Rattlers
AZ Cardinals Cheerleaders
Farmers Insurance
Village Health Club and Spa
Microsoft Corporation
Lifetime Fitness
CrackerJax
PepsiCo
Philosophy
Arizona Kidney Foundation Charity Function
Arizona Fight Night Charity Event
Home Depot
Mattress Firm
Papa Johns Pizza
American Express
Wells Fargo Bank
Forever 21
Intel Corporation
Hacienda Inc.
KZZP (KISS) FM
Damon Williams Associates
Jordan Engineering Group
Styles 4 Less
AT&T
Motorola
Prudential
Heritage Trucking
Dillards
Wilson & Company
Desert Mountain Properties
Red Mountain Weight Loss Centers
Living Spaces
Plexus Worldwide</p>
<h2>We offer These Services:</h2>
<p>DJ Music Entertainment – Professional sound systems and wireless microphones
Photographers
Videographers
Photo Booths, Video Booths and Green Screen
Photo Montage / Slide Shows
Large Video Screens/ LED video walls / HD TVs
Truss Systems w/LED lighting
LED multi-color Up-lighting effects and room effects
Decorations/Banners/Red Carpet/Dance Floors
Furniture Rentals – Tables. Linens, chairs, podiums, staging, etc.
LED Glow tables (any color) w/ Logo branding and Leather chairs
Casino Games and Professional casino dealers
Complete Party Planning Services
Big or Small… Let us know what you need. We’ve worked with the largest corporations in the world from the NFL and Microsoft to small companies with a dozen employees. Our professional and experienced event planners can assist you to make your event a trouble-free success. Call us today for a FREE no-obligation consultation with one of our expert company/corporate planners at 480-947-6500.
</p>

<h2>Add a Photo Booth or Video Booth with Green Screen to your Function</h2>
<p>Party Masters has the BEST photo/video booth, with GREEN SCREEN, on the market! Our booths include the top of line Photography cameras for photos and videos in HD!  We include the fun PROPS and provide you with UNLIMITED photo sessions and custom designed printouts. Photo booths rentals in Phoenix are a huge hit with all age groups at any event. Book our fun photo booth at your next company function. It’s the most fun you will ever have with photos or videos. To learn more about our Photo Booth: Click Here
<br></br></p>
<h2>Add a Photo Montage to your Function</h2>
<p>Party Masters will create, produce and show your custom slide show Photo Montage (PixVid™) production, at your next company function, to share with all of your co-workers and guests. We’ll bring the projector(s), big screen(s), and connect to our sound system so everyone can enjoy your Photo Montage production. Call us for special discount pricing.
</p>
<h2>Add Karaoke to Your Function<:h2>
<p>It’s a blast! You and your guests can test out their vocal skills in front of everyone singing along to the thousands of instrumental titles in our Karaoke music library. We’ll bring a TV monitor so you can view and sing to the lyrics, the microphones, connected to our sound system, plus all of the music books so you and your guests can pick their favorite tune to sing! Fun for everyone!
Karaoke is an additional service to our standard DJ service. Equipment/service fees apply. Call for pricing.
</p>
<h2>Add Videography and/or Photography</h2>
<p>Party Masters Entertainment has in-house professional staff experts, photographers and videographers, to capture all of the highlights, speeches, and guests (photos and interviews) at your next Arizona corporate event. We have worked with hundreds of companies to help them with all of their video and photo needs. We can do the same for you.
<br></br>
Videography: We use only professional grade cameras (not consumer home video cameras you can buy at your local retail store). Our videographers have the visual expertise to get in the best position, inconspicuously, to capture the highlight at that moment without being in the way of your guests and other vendors. Your videographer(s) will capture the main attractions and events at your event. If requested, we can ask your guests to say a few words (interview) during/after your event.
<br></br>
Photography: Our professional photography staff have been working in the industry for many years. They know how to capture the highlights and sights at your next Arizona company event. Our cameras capture the highest resolution in HD, so all photos will look great at any size. Once your event concludes, we can deliver the disc of all of the raw photos immediately, or produce any brochure or layout you prefer. Just ask and we can do it
</p>
</div>

</div>
<div class="selectdate">
<h1>Check Availability and get a Price Quote</h1>
<a href="pricequote.php">Get a Free Price Quote</a>

</div>
<center>
<center>
<div id="blackfade">
<div id="1">
<div id="1" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('1');"><i class="fa fa-times-circle-o"></i></a>
<img src="wedding.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(2);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(13);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="2">
<div id="2" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('2');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(3);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(1);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="3">
<div id="3" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('3');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner2.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(4);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(2);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="4">
<div id="4" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('4');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner3.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(5);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(3);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="5">
<div id="5" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('5');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner4.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(6);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(4);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="6">
<div id="6" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('6');"><i class="fa fa-times-circle-o"></i></a>
<img src="box1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(7);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(5);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="7">
<div id="7" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('7');"><i class="fa fa-times-circle-o"></i></a>
<img src="boxbanner.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(8);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(6);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="8">
<div id="8" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('8');"><i class="fa fa-times-circle-o"></i></a>
<img src="img35.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(9);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(7);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="9">
<div id="9" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('9');"><i class="fa fa-times-circle-o"></i></a>
<img src="memories.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(10);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(8);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="10">
<div id="10" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('10');"><i class="fa fa-times-circle-o"></i></a>
<img src="w1.jpg">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(11);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(9);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="11">
<div id="11" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('11');"><i class="fa fa-times-circle-o"></i></a>
<img src="w1.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(12);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(10);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="12">
<div id="12" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('12');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(13);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(11);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>
<div id="13">
<div id="13" class="item_box">
<a href="javascript:void(0)" onclick="Hidebox('13');"><i class="fa fa-times-circle-o"></i></a>
<img src="banner4.png">
<h3>Head caption</h3>
<p>Sub Caption</p>
<a href="javascript:void(0)" onclick="Next(1);"><i id="right" class="fa fa-arrow-circle-o-right"></i></a>
<a href="javascript:void(0)" onclick="Prev(12);"><i id="left" class="fa fa-arrow-circle-o-left"></i></a>
</div>
</div>


</div>
</center>
<div class="gallery">
<h1>Gallery Sample for testing</h1>
<div id="center" class="gallerylist">
<ul>
<li>
<img onclick="Showbox('1');"  class="small" src="wedding.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('2');" class="small" src="banner1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('3');"  class="small" src="banner2.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('4');"  class="small" src="banner3.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('5');"  class="small" src="banner4.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('6');"  class="small" src="box1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('7');"  class="small" src="boxbanner.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('8');"  class="small" src="img35.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('9');"  class="small" src="memories.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('10');" class="small" src="w1.jpg"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('11');" class="small" src="banner.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('12');" class="small" src="w1.png"><i class="fa fa-search-plus"></i>
</li>
<li>
<img onclick="Showbox('13');" class="small" src="banner4.png"><i class="fa fa-search-plus"></i>
</li>
</ul>
</div>


<center>

<a href="gallery.php">Checkout Party Master full Gallery</a>
</div>

</center>
<footer>

<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>


</body>

</html>