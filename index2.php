<html>
<head>
<title>Party Masters AZ</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="main.css" type="text/css" media="all" />
<script src="jqueryslide.js"></script>
<script src="slidecontrol.js"></script>
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
    <script src="slider.js" type="text/javascript"></script>
	<script type="text/javascript"
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js">
    </script>
     <script type="text/javascript" language="javascript">
         $(function () {
             var $win = $(window);

             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                     document.getElementById('navback').style.opacity = 0.7;    
             });
			 
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logotop').style.opacity = 1;
			 });
			 var $win = $(window);
             $win.scroll(function () {
                 if ($win.scrollTop() == 0)
                 document.getElementById('logoscroll').style.opacity = 0;
			 });
         });
    </script>
</head>

<body>
<header>
<div class="header2">
<img id="navback" src="headerback.png"/>
<center>
<nav>
<a id="Menu" style="cursor: pointer;" onclick="ShowNav();"><i class="fa fa-bars"></i> Menu</a>
<a id="HideNav" style="cursor: pointer;" onclick="HideNav();"><i class="fa fa-bars"></i> Menu</a>
<div id="SmallNav">
<ul>
<li><a href="index.php">Home</a></li>
<li><a href="about.php">About Us</a></li>
<li><a href="pricequote.php">Price Quote</a></li>
<li><a href="gallery.php">Photo Gallery</a></li>
<li><a href="reviews.php">Reviews</a></li>
<li><a href="employment.php">Employment</a></li>
<li><a href="faq.php">Faqs</a></li>
<li><a href="contact.php">Contact US</a></li>
</ul>
</div>
</nav>

<a href="tel:1-480-947-6500" style="color:#29A2D2;"><img id="logoscroll" src="logoscroll.png"/></a>


<div class="logintop">
<a id="log" href="#"><i class="fa fa-sign-in" style="font-size:18px;color:white;margin-left:2px;margin-right:5px;"></i>Login</a>
</center>

</div>

</div>

<img id="logotop" src="logo.png"/>
</header>
<center>
<script type="text/javascript">
window.onscroll = function (e){
document.getElementById('navback').style.opacity = 1;
document.getElementById('logotop').style.opacity = 0;
document.getElementById('logoscroll').style.opacity = 1;
} 
</script>

<div id="container">
	<ul>
      	<li><img src="banner.png" width="100%" /><h3>We Make Memories Happen</h3><h5>sub caption</h5></li>
      	<li><img src="banner1.jpg" width="100%" /><h3>Voted Arizona's Best!</h3><h5>sub caption</h5></li>
		<li><img src="banner2.jpg" width="100%" /><h3>Your Event Customized!</h3><h5>sub caption</h5></li>
		<li><img src="banner3.jpg" width="100%" /><h3>Professional Event Experts</h3><h5>sub caption</h5></li>
		<li><img src="banner4.jpg" width="100%" /><h3>Professional - Experienced - Creative</h3><h5>sub caption</h5></li>

      </ul>
</div>

<div class="Featuredbanner">

</div>

<div class="Featuredbannertxt">
<a href="http://client.partymastersaz.com/availability/index.asp"><h2><i id="light" class="fa fa-caret-right"></i> Start Right Here!</h2></a>
<h1>
Speak to a Professional Entertainment Specialist now: <a href="tel:1-480-947-6500" style="color:#29A2D2;">(480) 947-6500<a></span></h1>
</div>
<script type="text/javascript">
(function(){
var i = 1;
var start = true;   
    setInterval(function(){
        if (i>0&&start){
         var o = $("#light").css("opacity");
        
            var s = parseFloat(o) - 0.6;
       
            $("#light").css("opacity", s.toString());
           i = s;    
        }
    
        else {
        start = false;
         var o = $("#light").css("opacity");
        var s = parseFloat(o) + 0.6;
       
         $("#light").css("opacity", s.toString());
        i = s; 
            if(s > 1) start = true;
        
        
        }
        
   
   
   
    }, 200);

 
})();

</script>

<div class="hotbutton">
<a href="http://client.partymastersaz.com/availability/index.asp">Free Price Quote</a>
</div>
<div class="social">
<ul>
<li><img src="fb64.png"></li>
<li><img src="gp64.png"></li>
<li><img src="ytb64.png"></li>
<li><img src="twit64.png"></li>
</ul>
</div>
<div class="services">

<h3><span style="color:white;"> We provide </span>ALL<span style="color:white;"> the Entertainment Services you need for</span> UNFORGETTABLE<span style="color:white;"> Events and Parties!</span></h3>
</div>
<div class="iconsbackground">
<img src="redback.png"/>
</div>


<div class="iconslist">
<ul>
<a href="photo_booth.php"><li id="booth">
<h1 id="booth">Photo Booths</h1>
<img id="iconround" src="iconset/booth.png"/>
<p>this is the icon's description.<br> This icon is just so cool!</p>
</li></a>

<a href="discjockeys.php"><li id="disc">
<h1 id="disc">Disc Jockeys</h1>
<img id="iconround" src="iconset/dj.png"/>
<p>this is the icon's description.<br> This icon is just so cool!</p>
</li></a>

<a href="photographers.php"><li id="cam">
<h1 id="cam">Photographers</h1>
<img id="iconround" src="iconset/camera.png"/>
<p>this is the icon's description.<br> This icon is just so cool!</p>
</li></a>
</ul>
</div>
<div id="secondline" class="iconslist">
<ul>
<a href="led_lighting.php"><li id="led">
<h1 id="led">LED Lighting</h1>
<img id="iconround" src="iconset/led.png"/>
<p>this is the icon's description.<br> This icon is just so cool!</p>
</li></a>

<a href="photo_montage.php"><li id="mon">
<h1 id="mon">Photo Montage</h1>
<img id="iconround" src="iconset/montage.png"/>
<p>this is the icon's description.<br> This icon is just so cool!</p>
</li></a>
<a href="videographers.php"><li id="vid">
<h1 id="vid">Videographers</h1>
<img id="iconround" src="iconset/video.png"/>
<p>this is the icon's description.<br> This icon is just so cool!</p>
</li></a>
</ul>
</div>

<div id="thirdline" class="iconslist">
<ul>
<a href="event_rentals.php"><li>
<h1>Event Rentals</h1>
<img id="iconround" src="iconset/party.png"/>
</li></a>

<a href="dance_floors.php"><li>
<h1>Dance Floors</h1>
<img id="iconround" src="iconset/dance.png"/>
</li></a>
<li>

<a href="screen_projectors.php"><h1>Screens Projectors</h1>
<img id="iconround" src="iconset/projectors.png"/>

</li>
<a href="party_planning.php"><li>
<h1>Party Planning</h1>
<img id="iconround" src="iconset/partyplan.png"/>
</li></a>
</ul>
</div>
<div class="photobooth">
<h1>Check Out Our Awesome<br><span style="font-size:28px;"> PHOTO BOOTH</span></h1>
<i class="fa fa-camera" style="font-size:85px;color:white;"></i>
<h2>The Party Masters Photo Booth is the most popular photo booth in Phoenix.<br>
</h2><br>
<a href="#">See More Details</a>
<br><br><br>
</div>
<div class="headline">
<h1>Welcome to Party Masters Events + Services</h1>
<p><span style="color:#29A2D2;font-size:18px;">Party Masters Entertainment</span> 
provides professional on-location mobile Disc Jockey, Videography, Photography, Photo booth 
Rentals, Video booth rentals with Green Screen, Photo Montage – Slide Show services (PixVid™), Party Rentals & Party Planning 
services for any occasion throughout Arizona. We also offer specialty items such as: LED uplighting effects, Photo Booths, 
Green screen, magicians, jazz bands, and a full array of LED up-lighting effects for any occasion.<br><br> 

<span style="color:#D72323;font-size:18px;">Our Phoenix DJ service and Phoenix Party Planners,</span>
 made up of professional entertainers and experienced party planners, have thousands of events under 
our belts, for over 25 years. Plus, our AWARD WINNING First-Class customer service, for on-site party planning and party rental
 services, has helped us to become the most referred entertainment and party planning and party rental service company among 
 resorts, party planners, wedding planners, consultants, and previous customers.<br><br> 
<span style="color:#6DDC48;font-size:18px;">Get a FREE price quote</span> 
and check your event date availability and schedule your FREE No-Obligation personal consultation meeting 
with one of our in-house entertainment sales team members.</p>
</div>
<div class="browselist">
<h1>Your Event Type</h1>
<ul>
<li><a href="weddings.php"><i class="fa fa-search-plus"></i> Weddings</a></li>
<li><a href="birthdays.php"><i class="fa fa-search-plus"></i> Birthdays</a></li>
<li><a href="anniversaries.php"><i class="fa fa-search-plus"></i> Anniversairies</a></li>
<li><a href="mitzvahs.php"><i class="fa fa-search-plus"></i> Mitzvahs</a></li>
<li><a href="corporate.php"><i class="fa fa-search-plus"></i> Company/Corporate</a></li><br>
<li><a href="casino.php"><i class="fa fa-search-plus"></i> Casino Games</a></li>
<li><a href="school.php"><i class="fa fa-search-plus"></i> School Events</a></li>
<li><a href="graduation.php"><i class="fa fa-search-plus"></i> Graduations</a></li>
<li><a href="reunions.php"><i class="fa fa-search-plus"></i> Reunions</a></li>
<li><a href="sweet16.php"><i class="fa fa-search-plus"></i> Super Sweet 16</a></li><br>
<li><a href="college.php"><i class="fa fa-search-plus"></i> College Events</a></li>
<li><a href="kidsparties.php"><i class="fa fa-search-plus"></i> Kids Parties</a></li>
<li><a href="parivate.php"><i class="fa fa-search-plus"></i> Private Parties</a></li>
<li><a href="retirement.php"><i class="fa fa-search-plus"></i> Retirement</a></li>
<li><a href="bridalshowers.php"><i class="fa fa-search-plus"></i> Bridal Showers</a></li><br>
<li><a href="grandopenings.php"><i class="fa fa-search-plus"></i> Grand Openings</a></li>
<li><a href="themeparties.php"><i class="fa fa-search-plus"></i> Theme Parties</a></li>
<li><a href="houseparties.php"><i class="fa fa-search-plus"></i> House Parties</a></li>
<li><a href="holidayparties.php"><i class="fa fa-search-plus"></i> Holiday Parties</a></li>
<li><a href="fundraisers.php"><i class="fa fa-search-plus"></i> Fundraisers</a></li>
<li><a href="karaoke.php"><i class="fa fa-search-plus"></i> Karaoke</a></li><br>
<li><a href="other.php"><i class="fa fa-search-plus"></i> Other Events</a></li>
</ul>

</div>
<div class="boxtop">
<image src="memories.jpg">
<h1>We Make Memories Happen</h1>
<p>We understand that your special event is a once-in-a-lifetime celebration—whether it’s your wedding day,
anniversary, a monumental birthday celebration or corporate event. When you choose Party Masters Entertainment
to entertain at your event,or help plan your event, you are choosing a partner to help make your event 
as memorable as possible. 
</p>

</div>
<div class="boxpro">
<image src="pro.png">
<h1>Professional</h1>

<p><br>Our Arizona DJ’s pride themselves on being professional and working closely with other vendors at your event. Each local Arizona DJ & Videographer hired is “PME Certified”, meaning our wedding DJ’s and DJ’s for all services have the talent, experience, professionalism, passion, music knowledge,
 to meet our high standards at Party Masters Entertainment.</a></p>

</div>
<script type="text/javascript">
function show_more()
{
document.getElementById('shortp').style.opacity = 0;
document.getElementById('fullp').style.style.p = 1; 

}

</script>
<div class="box">
<image src="affordable.png">
<h1>Affordable Pricing</h1>

<p><br>Party Masters’ Phoenix DJ service has the most competitive and attractive prices in Arizona. 
Hire us for multiple event planning services in Phoenix... <a href="">Show More</a></p>

</div>
<div id="blackfade">
<center>
<div id="q1">
<div class="qualitybox">
<a href="javascript:void(0)" onclick="Collapsebox('q1');"><i class="fa fa-times-circle-o"></i></a>
<h1>Experienced</h1>
<p>Party Masters Entertainment Arizona DJ services have the expertise, knowledge, professionalism and most importantly the experience to select the right party music and get your guests up and dancing. We do our best to get everyone involved in your party celebration. Party Masters Entertainment has experience since 1983 and is one of the most reputable and trusted Phoenix DJ services in Arizona. Our Phoenix DJ’s have the most experience combined than any other local Phoenix DJ service in Arizona. We have performed all over the state of Arizona, so if you’re searching for a Phoenix DJ, Scottsdale DJ, Tempe DJ, Chandler DJ, Gilbert DJ, Mesa DJ, Glendale DJ, Paradise Valley DJ, Peoria DJ, Sedona DJ, Prescott DJ, or Arizona DJ service in your Arizona city, Party Masters Entertainment covers the entire state of Arizona.
</p>
</div>
</div>
<div id="q2">
<div class="qualitybox">
<a href="javascript:void(0)" onclick="Collapsebox('q2');"><i class="fa fa-times-circle-o"></i></a>
<h1>Reliable</h1>
<p>Party Masters Entertainment Arizona DJ services have the expertise, knowledge, professionalism and most importantly the experience to select the right party music and get your guests up and dancing. We do our best to get everyone involved in your party celebration. Party Masters Entertainment has experience since 1983 and is one of the most reputable and trusted Phoenix DJ services in Arizona. Our Phoenix DJ’s have the most experience combined than any other local Phoenix DJ service in Arizona. We have performed all over the state of Arizona, so if you’re searching for a Phoenix DJ, Scottsdale DJ, Tempe DJ, Chandler DJ, Gilbert DJ, Mesa DJ, Glendale DJ, Paradise Valley DJ, Peoria DJ, Sedona DJ, Prescott DJ, or Arizona DJ service in your Arizona city, Party Masters Entertainment covers the entire state of Arizona.
</p>
</div>
</div>
<div id="q3">
<div class="qualitybox">
<a href="javascript:void(0)" onclick="Collapsebox('q3');"><i class="fa fa-times-circle-o"></i></a>
<h1>Dependable</h1>
<p>Our Arizona DJ service has been the most dependable DJ service in Phoenix and all cities in Arizona. We only staff top individuals that care and have passion for music, video and the party planning entertainment business. Our Arizona Disc Jockey services, Arizona Videography services, and Phoenix Arizona Photography serviceshave a full emergency back-up system and a qualified person standing by to assure you that your event will not be missed. Not many other services can provide this, especially individual Arizona DJ services working from home.
</p>
</div>
</div>
<div id="q4">
<div class="qualitybox">
<a href="javascript:void(0)" onclick="Collapsebox('q4');"><i class="fa fa-times-circle-o"></i></a>
<h1>Talented</h1>
<p>Any Phoenix DJ service in Arizona can claim they have talent. Party Masters can prove it! Just make an appointment at our office Click here, and we’ll show you video highlights of our talented staff of Party Planners, Arizona DJ entertainers , Arizona Videographers, Phoenix Photographers, Arizona dancers, band singers, magicians in Phoenix, and many others.
</p>
</div>
</div>
<div id="q5">
<div class="qualitybox">
<a href="javascript:void(0)" onclick="Collapsebox('q5');"><i class="fa fa-times-circle-o"></i></a>
<h1>Interactive</h1>
<p>Our professional Phoenix DJ’s and entertainers know when to interact with you and your guests without going too far. They do their best to get everyone involved in your celebration. We Make It Happen!
</p>
</div>
</div>
<div id="q6">
<div class="qualitybox">
<a href="javascript:void(0)" onclick="Collapsebox('q6');"><i class="fa fa-times-circle-o"></i></a>
<h1>Quality</h1>
<p>We use only the best professional equipment available today. All of our sound, lights and video equipment is new and is updated as needed to bring you the best overall sound, lighting, and video production possible for any event small or large.
</p>
</div>
</div>
<div id="q7">
<div class="qualitybox">
<a href="javascript:void(0)" onclick="Collapsebox('q7');"><i class="fa fa-times-circle-o"></i></a>
<h1>Recommended & Trusted</h1>
<p>Party Masters’ Phoenix DJ service, Photography service, Videography service, Party Rentals & Party Planning services in Arizona are highly recommended and recognized by Phoenix resorts, hotels, catering halls, and is the most trusted Phoenix Wedding DJ and Phoenix Photography and Arizona Videography service each time we are hired. We are the most recommended Arizona DJ service company and party rentals service in Phoenix, in the entire state. If you’re looking for a Wedding DJ service in Arizona for your Wedding Reception or Wedding Ceremony, Company/Corporate event, Birthday, Anniversary, Bar Mitzvah, Bat Mitzvah, School Dance, or Private Party, Party Masters Entertainment is your choice for the best Arizona Disc Jockey service for your music entertainment and the most trusted and respected Phoenix Photography service, in addition to the best reason to hire a Videographer in Arizona, with Party Masters Entertainment.
</p>
</div>
</div>
<div id="q8">
<div class="qualitybox">
<a href="javascript:void(0)" onclick="Collapsebox('q8');"><i class="fa fa-times-circle-o"></i></a>
<h1>Acknowledge</h1>
<p>With a combined experience at over 20,000 events over the past three decades, Party Masters Entertainment has been nominated for (acknowledged as) Arizona’s best Wedding DJ service. Our customer service, Phoenix Wedding DJ service, Photographers in Phoenix, Videography service in Arizona, Party Rentals in Phoenix and Party Planning services in Arizona, have created long lasting memories for all who attend our events.
</p>
</div>
</div>
</center>
</div>
<div class="flatcontent">

<script language="javascript" type="text/javascript">

function Expandbox(elementid)
{
document.getElementById(elementid).getElementsByClassName("qualitybox")[0].style.visibility = "visible";
document.getElementById(elementid).getElementsByClassName("qualitybox")[0].style.opacity = 1;

document.getElementById("blackfade").style.visibility = "visible";
document.getElementById("blackfade").style.opacity = 1;

}
function Collapsebox(elementid)
{
document.getElementById(elementid).getElementsByClassName("qualitybox")[0].style.visibility = "hidden";
document.getElementById(elementid).getElementsByClassName("qualitybox")[0].style.opacity = 0;
document.getElementById("blackfade").style.visibility = "hidden";


}


</script>


<h1>Party Masters Events + Services</h1>
<h5>Voted Arizona's best!</h5>
<ul>

<li onclick="Expandbox('q1');"><h1>Experienced</h1></li>
<li onclick="Expandbox('q2');"><h1>Reliable</h1></li>
<li onclick="Expandbox('q3');"><h1>Dependable</h1></li>
<li onclick="Expandbox('q4');"><h1>Talented</h1></li>

</ul>
<ul id="row2">
<li onclick="Expandbox('q5');"><h1>Interactive</h1></li>
<li onclick="Expandbox('q6');"><h1>Quality</h1></li>
<li onclick="Expandbox('q7');"><h1>Recommended & Trusted</h1></li>
<li onclick="Expandbox('q8');"><h1>Acknowledged</h1></li>
</ul>
</div>
<hr>



<div class="galleryhome">

<h1>A beautiful Gallery, Made of Memorable, Fun and Happy events!</h1>
<img src="gallery.png" />



<img id="gallerylinks" src="grayback.png">
<h3 id="gallerylinks">Events that you can't forget</h3>
<a id="gallerylinks" href="">Party Masters' Gallery</a>

</div>
<footer>
<img id="background" src="footerback.png"/>
<div class="footer">
<h1>Party Masters Events + Services</h1>
<a id="pricequote" href="#">Get a Free Price Quote</a>
<a id="phone" href="tel:1-480-947-6500"><i class="fa fa-phone-square"></i> Call us: (480) 947-6500</a><br>
<a id="mail" href="mailto:info@partymastersaz.com"><i class="fa fa-envelope"></i> Info@PartyMastersAZ.com</a>

<ul id="socialf">
<li><a href="https://www.facebook.com/"><img src="fb64.png"/></a></li>
<li><a href=""><img src="gp64.png"/></a></li>
<li><a href=""><img src="ytb64.png"/></a></li>
<li><a href=""><img src="twit64.png"/></li>
</ul>
<p>Party Masters Entertainment is locally owned and operated, so we take the utmost care in each Arizona event.
 We look forward to meeting you and adding you to our growing family of satisfied customers.
Find us everywhere! Locate us in the most popular online directories under Wedding DJ’s in Arizona,
 Birthday party DJ’s in Arizona, Anniversary DJ’s in Arizona, Company event DJ’s Arizona, Corporate
 event DJ’s in Arizona, & private party DJ’s in Arizona; in DiscJockeys.com, MyPartyPlanner.com,
 PartyBlast.com, PartyPlannerUSA.com, ThePartyNetwork.com, and many other top entertainment directories.
</p>
<h2>We accepts all MAJOR CREDIT CARDS, Checks and PayPal.</h2>
<ul id="payment"><li id="paypal"><img id="paypal" src="paypal.png"></li><li><img src="visa.png"></li><li><img src="master.png"></li><li><img src="express.png"></li><li><img src="discover.png"></li></ul>
</div>
<div class="footerbottom">
<h1>Copyright © 2004-2015 Party Masters Events + Services. All rights reserved.</h1>
</div>
</footer>
</center>


</body>

</html>